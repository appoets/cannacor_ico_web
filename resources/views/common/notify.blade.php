@if (count($errors) > 0)
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
       <div style="margin-top: 25px;">
           <div class="alert alert-danger" style="position: initial;">
               <button type="button" class="close" data-dismiss="alert">×</button>
               <ul>
                   @foreach ($errors->all() as $error)
                       <li>{{ $error }}</li>
                   @endforeach
               </ul>
           </div>
       </div>
    </div>
  </div>
</div>
@endif

@if(Session::has('flash_error'))
<div class="container-fluid">
  <div class="row">
     <div class="col-md-12">
         <div style="margin-top: 25px;" style="position: initial;">
             <div class="alert alert-danger">
                 <button type="button" class="close" data-dismiss="alert">×</button>
                 {{ Session::get('flash_error') }}
             </div>
         </div>
     </div>
  </div>
</div>
@endif


@if(Session::has('flash_success'))
<div class="container-fluid">
  <div class="row">
     <div class="col-md-12">
         <div style="margin-top: 25px;" style="position: initial;">
             <div class="alert alert-success">
                 <button type="button" class="close" data-dismiss="alert">×</button>
                 {{ Session::get('flash_success') }}
             </div>
         </div>
     </div>
  </div>
</div>
@endif

@if(Session::has('flash_warning'))
<div class="container-fluid">
  <div class="row">
     <div class="col-md-12">
         <div style="margin-top: 25px;" style="position: initial;">
             <div class="alert alert-warning">
                 <button type="button" class="close" data-dismiss="alert">×</button>
                 {{ Session::get('flash_warning') }}
             </div>
         </div>
     </div>
  </div>
</div>
@endif

@if(Session::has('flash_notverified'))
<div class="container-fluid">
  <div class="row">
     <div class="col-md-12">
         <div style="margin-top: 25px;" style="position: initial;">
             <div class="alert alert-danger">
                 <button type="button" class="close" data-dismiss="alert">×</button>
                 {{ Session::get('flash_notverified') }} <a href="{{ url('/resend') }}"><button class="btn btn-primary">Resend Verification</button></a>
             </div>
         </div>
     </div>
  </div>
</div>
@endif