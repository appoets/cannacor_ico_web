@extends('coinadmin.layout.base')

@section('title', 'Update Profile ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">

			<h5 style="margin-bottom: 2em;">Update Profile</h5>

            <form class="form-horizontal" action="{{route('coinadmin.profile.update')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}

				<div class="form-group row">
					<label for="name" class="col-xs-2 col-form-label">Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ Auth::guard('coinadmin')->user()->name }}" name="name" required id="name" placeholder=" Name">
					</div>
				</div>

				<div class="form-group row">
					<label for="email" class="col-xs-2 col-form-label">Email</label>
					<div class="col-xs-10">
						<input class="form-control" type="email" required name="email" value="{{ isset(Auth::guard('coinadmin')->user()->email) ? Auth::guard('coinadmin')->user()->email : '' }}" id="email" placeholder="Email">
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-2 col-form-label">Picture</label>
					<div class="col-xs-10">
						@if(isset(Auth::guard('coinadmin')->user()->picture))
	                    	<img style="height: 90px; margin-bottom: 15px; border-radius:2em;" src="{{img(Auth::guard('coinadmin')->user()->picture)}}">
	                    @endif
						<input type="file" accept="image/*" name="picture" class=" dropify form-control-file" aria-describedby="fileHelp">
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update Profile</button>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>


<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">

			<h5 style="margin-bottom: 2em;">BTC Account</h5>

            <form class="form-horizontal" action="{{route('coinadmin.overallBTC')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}

            	<div class="form-group row">
					<label for="name" class="col-xs-2 col-form-label">Total BTC</label>
					<div class="col-xs-10">
						<!-- <input class="form-control" type="text" value="" name="btc" required id="name" placeholder="BTC Address"> -->
						{{$coin}}
					</div>
				</div>

				<div class="form-group row">
					<label for="name" class="col-xs-2 col-form-label">BTC Address</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="" name="btc" required id="name" placeholder="BTC Address">
					</div>
					
				</div>
				<div class="form-group row">
					<label for="name" class="col-xs-2 col-form-label">BTC value</label>
					
					<div class="col-xs-10">
						<input class="form-control" type="text" value="" name="amount" required id="amount" placeholder=" 1 BTC">
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>

					<div class="col-md-6" id="btc_otp_btn_div">
						<button type="button" class="btn btn-default" id="btc_otp_btn" onclick="myotpgenerate('btc');" >Generate OTP</button>
					</div>
					
					<div class="col-md-3 col-xs-10" id="btc_submit_div" style="display: none;">
						<input name="btc_otp" id="btc_otp" type="text" class="form-control" placeholder="Enter your OTP Here" required />
						<br>
						<button type="submit" class="btn btn-primary" disabled id="btc_submit" >Submit</button>
					</div>

				</div>
			</form>
		</div>
    </div>
</div>





<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">

			<h5 style="margin-bottom: 2em;">ETH Account</h5>

            <form class="form-horizontal" action="{{route('coinadmin.withdrawETH')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}

				<div class="form-group row">
					<label for="eth" class="col-xs-2 col-form-label">ETH Address</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="" name="eth" required placeholder="ETH Address">
					</div>
				</div>
	            <table class="table table-striped table-bordered dataTable" id="table-2">
	                <thead>
	                    <tr>
	                        <th>ID</th>
	                        <th>User</th>
	                        <th>Payment Method</th>
	                        <th>{{ ico() }} Quantity</th>
	                        <th>{{ ico() }} Price</th>
	                        <th>Price</th>
	                    </tr>
	                </thead>
	                <tbody>
	                    @foreach($Transactions as $index => $transaction)
	                    <tr>
	                        <td>
	                        	<input type="checkbox" name="ids[]" value="{{$transaction->id}}">
	                        </td>
	                        <td>{{ $transaction->user->name }}</td>
	                        <td>{{ $transaction->payment_mode }}</td>
	                        <td>{{ $transaction->ico }}</td>
	                        <td>{{ currency($transaction->ico_price) }}</td>
	                        <td>{{ $transaction->price }}</td>
	                    </tr>
	                    @endforeach
	                </tbody>
	                <tfoot>
	                    <tr>
	                        <th>ID</th>
	                        <th>User</th>
	                        <th>Payment Method</th>
	                        <th>{{ ico() }} Quantity</th>
	                        <th>{{ ico() }} Price</th>
	                        <th>Price</th>
	                    </tr>
	                </tfoot>
	            </table>

	            <div class="col-md-6 form-group row" id="eth_otp_btn_div">
					<button type="button" class="btn btn-default" id="btc_otp_btn" onclick="myotpgenerate('eth');" >Generate OTP</button>
				</div>

				<div class="form-group row" id="eth_submit_div" style="display: none;">
					<div class="col-md-3">						
					<input name="eth_otp" id="eth_otp" type="text" class="form-control" placeholder="Enter your OTP Here" required />
					<button type="submit" class="btn btn-primary" disabled id="eth_submit" >Submit</button>
					</div>
				</div>
				<br>
				<br>

				<!-- <div class="form-group row">					
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div> -->

			</form>
		</div>
    </div>
</div>

<script type="text/javascript">
	function myotpgenerate(ctype){
		$.ajax({
		      url: "{{url('/coinadmin/btcotpgenerate')}}/"+ctype,
		      type: "GET",
		      //data:{'usd':$('#priceusd').val(),'price':$('.price').val()}
	  	}).done(function(response){

	  		$('#'+ctype+'_otp_btn_div').css('display','none');
	  		$('#'+ctype+'_submit_div').css('display','block');
	  		$('#'+ctype+'_submit').attr('disabled', false);

	  	}).fail(function(jqXhr,status){

	  	});

	}
</script>

@endsection
