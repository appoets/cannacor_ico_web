@extends('coinadmin.layout.base')

@section('title', 'History')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
         
            <h3> Transaction History</h3>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date/Time</th>
                        <th>Address</th>
                        <th>Amount</th>
                        <th>Payment</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($History as $index => $history)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                         <td>{{ date('d M Y H:i:s', strtotime($history->created_at)) }}</td>
                        <td>{{ $history->address }}</td>
                        <td>{{ $history->amount }}</td>
                        <td>{{ $history->payment }}</td>

                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                       <th>ID</th>
                        <th>Date/Time</th>
                        <th>Address</th>
                         <th>Amount</th>
                        <th>Payment</th>
                        

                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection