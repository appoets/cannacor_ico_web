@extends('layouts.login')

@section('content')

<style type="text/css">
#country{
    height: 50px;
    border-radius: 25px;
    text-align: center;
    width: 100%;
    display: inline-block;
}
</style>

<div class="signup">
   <!--  <h4>Invest Now <b>Save 15%</b> Limited Time Only! </h4> -->
<div class="container">
        <div class="pull-right col-lg-4">
            <div class="panel panel-default signupBox">
                <!-- <div class="panel-heading">Register</div> -->

                <div class="panel-body">
                    <a class="logo navbar-brand" href="{{ url('/') }}">
                       <img src="{{ Setting::get('site_logo') }}" height="50" alt="{{ Setting::get('site_title') }}">
                    </a>

                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        @if(Request::get('referral'))
                            <input type="hidden" name="referral" value="{{ Request::get('referral') }}">
                        @endif

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name"  class="control-label">Name</label>

                            <div class="">
                                <input id="name" type="text"  class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail Address</label>

                            <div class="">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required maxlength="50" >

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">Password</label>

                            <div class="">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="control-label">Confirm Password</label>

                            <div class="">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="country" class="control-label">Country</label>

                            <div class="">
                                <select id="country" class="form-control" name="country_id" required>
                                    <option value="" >Select your country</option>
                                    @foreach($country as $value)
                                      <option value="{{$value->id}}" >{{$value->country_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                      <!--   <div class="form-group">
                            <div class="">
                                <div class="checkbox">
                                    <div class="g-recaptcha" data-sitekey="6LeipUsUAAAAABAnNhG5giTpQjJhc661UZS7VTN0"></div>
                                </div>
                            </div>
                        </div> -->

                        <div class="form-group">
                            <div style="display: flex;">
                                <input type="checkbox" id="confirm_terms" name="" required style="width: 46px;margin-left: 10px;" />
                                <label style="" for="confirm_terms" > I accept <a href="{{url('/terms')}}" target="_blank" class="bflogLink"> terms & conditions </a></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary bflogBtn">
                                    Register
                                </button>
                            </div>
                        </div>
                    
                        <div class="form-group bypass-pages">
                            <p>Already have an account? <a href="{{url('/login')}}" class="bflogLink">Login</a></p>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    <script>
    $("form").submit(function(event) {

       var recaptcha = $("#g-recaptcha-response").val();
       if (recaptcha === "") {
          event.preventDefault();
          alert("Please check the recaptcha");
       }
    });
    </script>

    <script type="text/javascript">
        $("#name").keypress(function(e){

   if (window.event)
       code = e.keyCode;
   else
       code = e.which;
   if(code == 32 || (code>=97 && code<=122)|| (code>=65 && code<=90))
       return true;
   else
       return false;
});
    </script>
@endsection