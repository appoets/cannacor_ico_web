@extends('layouts.login')

@section('content')
<style type="text/css">


</style>

<div class="login">
   <!--  <h4>Crowdfunding starts <b>May 2018</b></h4> -->
   <section class="banner-sec banner-v2 banner-v3 section-pt-60">
   <div class="banner-item">
        <div class="banner-table">
           <div class="banner-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="banner-content">
                                <h1 class="banner-title">CannaCor: Where Cannabis Cultivation and Blockchain Technology Combine Seamlessly</h1>
                                <p>
                                    CannaCor and Blockchain Corporation are collaborating with a common vision to bring lasting change in the field of cannabis research, cultivation, processing and distribution. Our goal is to transform the entire cannabis cultivation process to make it as transparent and secure as possible.
                                </p>
                                <a href="http://cannacor.io/" class="btn btn-v3" target="_blank">Home</a>
                                <a href="doc/faq.pdf" target="_blank" class="btn btn-v3">F.A.Q</a>
                                <a href="doc/whitepaper-document.pdf" target="_blank" class="btn btn-v3">Download Whitepaper V4.6</a>
                                <div class="row baner-img">
                                    <div class="col-lg-10 mx-auto">
                                      <img src="img/CannaCor-Pty-light.png">
                                      <a href="https://blockchaincorporation.org/" target="_blank"><img src="img/Blockchain-small.png"></a>
                                    </div>
                                </div>
                                <div class="social-links">
                                <a href="https://www.facebook.com/cannacor" target="_blank" class="icon-button twitter"><i class="fa fa-facebook"></i><span></span></a>
<a href="https://twitter.com/canna_cor" target="_blank" class="icon-button facebook"><i class="fa fa-twitter"></i><span></span></a>
<a href="https://t.me/cannacor" target="_blank" class="icon-button google-plus"><i class="fa fa-telegram"></i><span></span></a>
<a href="https://www.linkedin.com/company/canna-cor-pty-ltd/" target="_blank" class="icon-button youtube"><i class="fa fa-linkedin"></i><span></span></a>
<a href="https://medium.com/@CannaCor" target="_blank" class="icon-button pinterest"><i class="fa fa-medium"></i><span></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-offset-1 col-lg-4">
            <div class="panel panel-default loginBox">
                <!-- <div class="panel-heading">Login</div> -->

                <div class="panel-body">
                    <a class="logo navbar-brand" href="{{ url('/') }}">
                       <img src="{{ Setting::get('site_logo') }}" height="50" alt="{{ Setting::get('site_title') }}">
                    </a>
                    <form class="form-horizontal login-form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail Address</label>

                            <div class="">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">Password</label>

                            <div class="">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6LdmTrkUAAAAAJzMr1xTR8mdQKo_xYHnPLSQ1nHM"></div>
                        </div>
                        <!-- <div class="form-group">
                            <div class="">
                                <div class="checkbox">
                                    <div class="g-recaptcha" data-sitekey="6LeipUsUAAAAABAnNhG5giTpQjJhc661UZS7VTN0"></div>
                                </div>
                            </div>
                        </div> -->

                        <div class="form-group">
                            <div class="">
                                <button type="submit" id="login-btn" class="btn btn-primary bflogBtn">
                                    Login
                                </button>

                                <p><a class="btn btn-link forget-password" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a></p>
                            </div>
                        </div>

                        <div class="form-group bypass-pages">
                            <p>Don't have an account? <a href="{{url('/register')}}" class="bflogLink">Register</a></p>
                        </div>

                    </form>
                </div>
            </div>
        </div>
                    </div>
                </div><!--  .container END -->
           </div>
        </div>
   </div>
</section>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    // $("#login-btn").on("click", function(e){
        // e.preventDefault();

        // if (grecaptcha.getResponse() == ""){
            // alert("Please verify google captcha");
        // } else {
            // $(".login-form").submit();
        // }
    // });
</script>
@endsection