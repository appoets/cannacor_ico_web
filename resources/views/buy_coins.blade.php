@extends('layouts.app')

@section('content')

    <div class="buy-coins-section">
      <div class="container">
        <div class="row">
            <div class="buy-coins-content">
              <h4 class="buy-coins-title">How to Buy CannaCor Coins</h4>
              <div class="cannocar-steps">
                <ul>
                    <li><span class="bold step-tl">Step 1</span> Download and Install the Atomic wallet
    Follow this link to download the Atomic wallet: <a href="https://atomicwallet.io/downloads">https://atomicwallet.io/downloads</a> </li>
                    <li><span class="bold step-tl">Step 2</span> Create your Atomic Wallet 
    Follow this link for instructions: <a href="https://support.atomicwallet.io/article/7-how-to-set-up-a-wallet">https://support.atomicwallet.io/article/7-how-to-set-up-a-wallet</a> 
    <span class="note-txt1">NB!! Copy and paste your 12-word phrase in a safe place. This is used to recover your account.</span></li>
                    <li><span class="bold step-tl">Step 3</span> Buy Bitcoin or Ethereum 
    Follow this link for instruction: <a href="https://support.atomicwallet.io/article/8-how-to-deposit-funds-to-atomic-wallet">https://support.atomicwallet.io/article/8-how-to-deposit-funds-to-atomic-wallet</a> </li>
                    <li><span class="bold step-tl">Step 4</span> <span class="bold">Create your account on the CannaCor Platform</span> 
    Follow this link to create your account: <a href="https://ico.cannacor.io/register">https://ico.cannacor.io/register</a> </li>
                    <li><span class="bold step-tl">&nbsp;</span> a) Copy your Ethereum address from your Atomic wallet and add this to your profile <br> <br>Follow this link for instructions: <a href="https://support.atomicwallet.io/article/107-how-to-receive-an-asset-mobile">https://support.atomicwallet.io/article/107-how-to-receive-an-asset-mobile</a>
                    </li>
                    <li><span class="bold step-tl">&nbsp;</span> Follow steps 1 to 5 (ignore steps 6 to 8) on the link above, once you have successfully copied your Ethereum address, go to your profile on the CannaCor ICO dashboard and paste you Ethereum Address in the block under my ETH wallet </li>
                    <li><span class="bold step-tl">&nbsp;</span> b) Submit your KYC documents <br>Please upload, </li>
                    <li><span class="bold step-tl">&nbsp;</span> - A clear copy of your passport in JPEG format &lt;<span class="note-txt1">NB! Please note that your account will be locked until KYC verification was successful. Your KYC will be approved in a couple of hours.</span> </li>
                    <li><span class="bold step-tl">Step 6</span> How to purchase CannaCor (CANO) coins on the ICO dashboard? 
    After you have registered and submitted your KYC, go to the home page on the CannaCor dashboard and follow steps 1 to 4. </li>
                    <li><span class="bold step-tl">&nbsp;</span> - Select the currency (Bitcoin or Ethereum) that you have purchased in the Atomic wallet and click continue <br>
    - Type in the amount of coins you want to purchase and click next step <br>
    - Click on “copy to clipboard” <br>
    - Send the Ethereum or Bitcoin from your Atomic wallet to the copied address. <br></li>
                    <li><span class="bold step-tl">Step 7</span> How to send Bitcoin or Ethereum to the CannaCor ICO dashboard. 
    Follow this link for instructions: <a href="https://support.atomicwallet.io/article/76-how-to-send-an-asset-in-atomic-wallet">https://support.atomicwallet.io/article/76-how-to-send-an-asset-in-atomic-wallet</a> </li>
                </ul>
              </div>
            </div>
        </div>
      </div>
    </div>
@endsection

@section('styles')
<style type="text/css">
@media (max-width: 991px) {
    #myTable thead {
      display: none;
    }
    #myTable td {
      word-break: none;
    }
    #myTable td:nth-of-type(1):before { content: "S.No" ; }
    #myTable td:nth-of-type(2):before { content: "Name"; }
    #myTable td:nth-of-type(3):before { content: "Date"; }

    #myTable td:first-child.dataTables_empty {
      text-align:  center;
      width:  100%;
    }

    #myTable td:first-child.dataTables_empty:before {
      display:  none;
    }

    #myTable td::before {
      width: 25%;
      display: inline-block;
    }
    #myTable td {
      padding: 10px !important;
      width: 100%;
      display: inline-block;
      text-align: left;
    }
    .transaction_balance table tbody tr th, .transaction_balance table tbody tr td {
      border: 1px solid #cacaca;
    }
    #myTable td:last-child {
      border-bottom: 0 !important;
    }
    #myTable tbody tr {
      margin: 20px 0;
      display: inline-block;
      width: 100%;
      border: 1px solid #cacaca;
  }
  .transaction_balance table tbody tr th, .transaction_balance table tbody tr td {
      border-bottom: 1px solid #cecece !important;
  }
}
</style>
@endsection