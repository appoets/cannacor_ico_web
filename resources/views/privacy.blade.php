@extends('layouts.app')

@section('content')

    <div class="referral-section">
        <div class="container">
                    <div class="row">
                        {!!Setting::get('privacy')!!}
                    </div>
        </div>
    </div>
@endsection

@section('styles')
<style type="text/css">
@media (max-width: 991px) {
    #myTable thead {
      display: none;
    }
    #myTable td {
      word-break: none;
    }
    #myTable td:nth-of-type(1):before { content: "S.No" ; }
    #myTable td:nth-of-type(2):before { content: "Name"; }
    #myTable td:nth-of-type(3):before { content: "Date"; }

    #myTable td:first-child.dataTables_empty {
      text-align:  center;
      width:  100%;
    }

    #myTable td:first-child.dataTables_empty:before {
      display:  none;
    }

    #myTable td::before {
      width: 25%;
      display: inline-block;
    }
    #myTable td {
      padding: 10px !important;
      width: 100%;
      display: inline-block;
      text-align: left;
    }
    .transaction_balance table tbody tr th, .transaction_balance table tbody tr td {
      border: 1px solid #cacaca;
    }
    #myTable td:last-child {
      border-bottom: 0 !important;
    }
    #myTable tbody tr {
      margin: 20px 0;
      display: inline-block;
      width: 100%;
      border: 1px solid #cacaca;
  }
  .transaction_balance table tbody tr th, .transaction_balance table tbody tr td {
      border-bottom: 1px solid #cecece !important;
  }
}
</style>
@endsection