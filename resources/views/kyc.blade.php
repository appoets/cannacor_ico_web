@extends('layouts.app')

@section('content')

    <div class="kyc-details">
        <div class="container">
            <div class="transaction_balance">
                <div class="settings-content-wrap p-f-30">
                    <!-- Settings Section Starts -->

                    <div class="ethereum_wallet_alert_msg alert-info">
                        Don’t have an ERC20 Ethereum wallet? Please follow this link for detailed instructions.
                        <a href="buy_coins" target="_blank"><span class="btn btn-primary">Click Here</span></a>
                    </div>

                    <div class="msg">
                        Purchases cannot be made until KYC documents are Verified
                    </div>
                    <div class="set-section">
                        <h6 class="m-0 set-main-tit">KYC Details</h6>
                            <table class="table table-striped table-bordered dataTable" id="table-2">
                                @if($KycDocument != "")
                                    <tbody>                                        
                                    @foreach($KycDocument  as $doc)
                                        <tr>
                                            <td><a href="{{img($doc->url)}}" target="_blank">{{@$doc->document->name}}</a></td>
                                            <td>
                                               @if($doc->status == "PENDING")
                                                <i class="fa fa-check-circle-o" style="font-size:48px;color:blue"></i>
                                                @elseif($doc->status=="APPROVED")
                                                <i class="fa fa-check-circle-o" style="font-size:48px;color:green"></i>
                                                @else
                                                <i class="fa fa-ban" style="font-size:48px;color:red"></i>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                @endif
                            </table>
                        <!-- <div class="set-sec-inner">
                            <div class="set-block">
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <p class="set-txt"><b>First Name</b></p>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" value="John" name="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <p class="set-txt"><b>Last Name</b></p>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" value="Smith" name="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <p class="set-txt"><b>Valid Identity Card</b></p>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <!-- Settings Section Ends -->
                    <!-- Settings Section Starts -->
                    <div class="set-section">
                        <!-- <h6 class="m-0 set-main-tit"></h6> -->
                        <div class="set-sec-inner">
                            @if(count($Kyc))
                                <form method="POST" action="{{ url('/kyc') }}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <!-- Settings Block Starts -->
                                    @foreach($Kyc as $kyc)
                                        <div class="set-block">
                                            <div class="form-group row">
                                                <div class="col-sm-3">
                                                    <p class="set-txt"><b>{{@$kyc->name}}</b></p>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div id="kyc-image-preview-{{$kyc->id}}" class="kyc-image-preview">
                                                        <label for="kyc-image-upload-{{$kyc->id}}" class="kyc-image-label" id="kyc-image-label-{{$kyc->id}}">Upload Image</label>
                                                        <input type="file" name="image[{{$kyc->id}}]" class="kyc-image-upload" id="kyc-image-upload-{{$kyc->id}}" @if(Auth::user()->status == 1) disabled="disabled" @endif />
                                                    </div>
                                                    <p class="muted kyc-txt">Please make sure that the photo is complete and clearly visible, in JPG format.</p>
                                                </div>
                                                <div class="col-sm-4">
                                                    <h5 class="example-tit">Example</h5>
                                                    <div class="kyc-demo-img bg-img" style="background-image: url({{img($kyc->image)}});"></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    <!-- Settings Block Ends -->
                                    <div class="text-center common-button">
                                        <button type="submit" class="btn btn-primary btn-info-full next-step">Submit</button>
                                    </div>
                                    <!-- Settings Block Ends -->
                                </form>
                            @endif
                        </div>
                    </div>
                    <!-- Settings Section Ends -->
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).on("change", ".kyc-image-upload", function(e) {
        if(this.files[0].size > 500000)
        {
            alert("The file size is too larage"); 
            $(".kyc-image-upload").val("");            
        }
    });
</script>
@endsection