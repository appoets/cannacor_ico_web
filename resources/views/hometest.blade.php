@extends('layouts.app')

@section('content')



<div class="step_wizard">
    <div class="container">
        <div class="row">
    <div class="col-md-12">
        
<div class="ethereum_wallet_alert_msg alert-info">
                        Don’t have an ERC20 Ethereum wallet? Please follow this link for detailed instructions.
                        <a href="buy_coins" target="_blank"><span class="btn btn-primary">Click Here</span></a>
                    </div>
        
    </div>
</div>
        <div class="col-md-9 wizard">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab">
                            <span class="step_count c1"><span class="step_span">Step </span>1</span> <span>Payment Methods</span>
                        </a>
                    </li> 
                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab">
                            <span class="step_count c2"><span class="step_span">Step </span> 2</span> <span>Payment Setup</span>
                        </a>
                    </li>
                   <!--  <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab">
                            <span class="step_count c3"><span class="step_span">Step </span> 3</span> <span>Transaction Details</span>
                        </a>
                    </li> -->
                    <li role="presentation" class="disabled">
                            <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab">
                                <span class="step_count">3</span> <span>Payment Setup</span>
                            </a>
                        </li>
                        <li role="presentation" class="disabled">
                            <a href="#step4" data-toggle="tab" aria-controls="step4" role="tab">
                                <span class="step_count">4</span> <span>Transaction Details</span>
                            </a>
                        </li>

                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" role="tabpanel" id="step1">
                    <div class="row">
                        @foreach($cointype as $cointypes)
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center pay-method">
                            <input type="radio" name="coin-type" id="sell-{{$cointypes->id}}" value="{{$cointypes->symbol}}" style="display: none;" onclick="mycoinselect(this.value);">
                            


                            <label for="sell-{{$cointypes->id}}">
                                @if($cointypes->symbol == 'ETH')
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="256px" height="417px" viewBox="0 0 256 417" version="1.1" preserveAspectRatio="xMidYMid">
                                    <g>
                                        <polygon points="127.9611 0 125.1661 9.5 125.1661 285.168 127.9611 287.958 255.9231 212.32" />
                                        <polygon points="127.962 0 0 212.32 127.962 287.959 127.962 154.158" />
                                        <polygon points="127.9611 312.1866 126.3861 314.1066 126.3861 412.3056 127.9611 416.9066 255.9991 236.5866" />
                                        <polygon points="127.962 416.9052 127.962 312.1852 0 236.5852" />
                                        <polygon points="127.9611 287.9577 255.9211 212.3207 127.9611 154.1587" />
                                        <polygon points="0.0009 212.3208 127.9609 287.9578 127.9609 154.1588" />
                                    </g>
                                </svg>
                                @elseif($cointypes->symbol == 'XRP')
                                <svg id="xrp" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 196.22 212.82">
                                    <path class="cls-1" d="M431,153.4c-7.52-4.34-16-5.6-24.39-5.9-7-.25-17.55-4.76-17.55-17.57,0-9.54,7.74-17.23,17.55-17.57,8.39-.29,16.86-1.55,24.39-5.9A44.45,44.45,0,1,0,364.31,68c0,8.61,3.06,16.54,7,23.89,3.29,6.18,4.95,17.66-6.32,24.16-8.39,4.84-18.85,1.78-24.08-6.59-4.42-7.07-9.75-13.69-17.21-18a44.45,44.45,0,1,0,0,77c7.46-4.31,12.79-10.92,17.21-18,3.62-5.8,12.67-13.19,24.08-6.6,8.37,4.84,11,15.44,6.32,24.17-3.91,7.35-7,15.27-7,23.88A44.45,44.45,0,1,0,431,153.4Z" transform="translate(-256.99 -23.53)"/>
                                </svg>
                                @elseif($cointypes->symbol == 'BTC')
                                <svg id="bitcoin" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 475.074 475.074" style="enable-background:new 0 0 475.074 475.074;" xml:space="preserve">
                                    <path d="M395.655,249.236c-11.037-14.272-27.692-24.075-49.964-29.403c28.362-14.467,40.826-39.021,37.404-73.666   c-1.144-12.563-4.616-23.451-10.424-32.68c-5.812-9.231-13.655-16.652-23.559-22.266c-9.896-5.621-20.659-9.9-32.264-12.85   c-11.608-2.95-24.935-5.092-39.972-6.423V0h-43.964v69.949c-7.613,0-19.223,0.19-34.829,0.571V0h-43.97v71.948   c-6.283,0.191-15.513,0.288-27.694,0.288l-60.526-0.288v46.824h31.689c14.466,0,22.936,6.473,25.41,19.414v81.942   c1.906,0,3.427,0.098,4.57,0.288h-4.57v114.769c-1.521,9.705-7.04,14.562-16.558,14.562H74.747l-8.852,52.249h57.102   c3.617,0,8.848,0.048,15.703,0.134c6.851,0.096,11.988,0.144,15.415,0.144v72.803h43.977v-71.947   c7.992,0.195,19.602,0.288,34.829,0.288v71.659h43.965v-72.803c15.611-0.76,29.457-2.18,41.538-4.281   c12.087-2.101,23.653-5.379,34.69-9.855c11.036-4.47,20.266-10.041,27.688-16.703c7.426-6.656,13.559-15.13,18.421-25.41   c4.846-10.28,7.943-22.176,9.271-35.693C410.979,283.882,406.694,263.514,395.655,249.236z M198.938,121.904   c1.333,0,5.092-0.048,11.278-0.144c6.189-0.098,11.326-0.192,15.418-0.288c4.093-0.094,9.613,0.144,16.563,0.715   c6.947,0.571,12.799,1.334,17.556,2.284s9.996,2.521,15.701,4.71c5.715,2.187,10.28,4.853,13.702,7.993   c3.429,3.14,6.331,7.139,8.706,11.993c2.382,4.853,3.572,10.42,3.572,16.7c0,5.33-0.855,10.185-2.566,14.565   c-1.708,4.377-4.284,8.042-7.706,10.992c-3.423,2.951-6.951,5.523-10.568,7.71c-3.613,2.187-8.233,3.949-13.846,5.28   c-5.612,1.333-10.513,2.38-14.698,3.14c-4.188,0.762-9.421,1.287-15.703,1.571c-6.283,0.284-11.043,0.478-14.277,0.572   c-3.237,0.094-7.661,0.094-13.278,0c-5.618-0.094-8.897-0.144-9.851-0.144v-87.65H198.938z M318.998,316.331   c-1.813,4.38-4.141,8.189-6.994,11.427c-2.858,3.23-6.619,6.088-11.28,8.559c-4.66,2.478-9.185,4.473-13.559,5.996   c-4.38,1.529-9.664,2.854-15.844,4c-6.194,1.143-11.615,1.947-16.283,2.426c-4.661,0.477-10.226,0.856-16.7,1.144   c-6.469,0.28-11.516,0.425-15.131,0.425c-3.617,0-8.186-0.052-13.706-0.145c-5.523-0.089-9.041-0.137-10.565-0.137v-96.505   c1.521,0,6.042-0.093,13.562-0.287c7.521-0.192,13.656-0.281,18.415-0.281c4.758,0,11.327,0.281,19.705,0.856   c8.37,0.567,15.413,1.42,21.128,2.562c5.708,1.144,11.937,2.902,18.699,5.284c6.755,2.378,12.23,5.28,16.419,8.706   c4.188,3.432,7.707,7.803,10.561,13.134c2.861,5.328,4.288,11.42,4.288,18.274C321.712,307.104,320.809,311.95,318.998,316.331z" fill="#FFFFFF" />
                                </svg>
                                @elseif($cointypes->symbol == 'BCH')
                                <svg id="bitcoin" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 475.074 475.074" style="enable-background:new 0 0 475.074 475.074;" xml:space="preserve">
                                    <path d="M395.655,249.236c-11.037-14.272-27.692-24.075-49.964-29.403c28.362-14.467,40.826-39.021,37.404-73.666   c-1.144-12.563-4.616-23.451-10.424-32.68c-5.812-9.231-13.655-16.652-23.559-22.266c-9.896-5.621-20.659-9.9-32.264-12.85   c-11.608-2.95-24.935-5.092-39.972-6.423V0h-43.964v69.949c-7.613,0-19.223,0.19-34.829,0.571V0h-43.97v71.948   c-6.283,0.191-15.513,0.288-27.694,0.288l-60.526-0.288v46.824h31.689c14.466,0,22.936,6.473,25.41,19.414v81.942   c1.906,0,3.427,0.098,4.57,0.288h-4.57v114.769c-1.521,9.705-7.04,14.562-16.558,14.562H74.747l-8.852,52.249h57.102   c3.617,0,8.848,0.048,15.703,0.134c6.851,0.096,11.988,0.144,15.415,0.144v72.803h43.977v-71.947   c7.992,0.195,19.602,0.288,34.829,0.288v71.659h43.965v-72.803c15.611-0.76,29.457-2.18,41.538-4.281   c12.087-2.101,23.653-5.379,34.69-9.855c11.036-4.47,20.266-10.041,27.688-16.703c7.426-6.656,13.559-15.13,18.421-25.41   c4.846-10.28,7.943-22.176,9.271-35.693C410.979,283.882,406.694,263.514,395.655,249.236z M198.938,121.904   c1.333,0,5.092-0.048,11.278-0.144c6.189-0.098,11.326-0.192,15.418-0.288c4.093-0.094,9.613,0.144,16.563,0.715   c6.947,0.571,12.799,1.334,17.556,2.284s9.996,2.521,15.701,4.71c5.715,2.187,10.28,4.853,13.702,7.993   c3.429,3.14,6.331,7.139,8.706,11.993c2.382,4.853,3.572,10.42,3.572,16.7c0,5.33-0.855,10.185-2.566,14.565   c-1.708,4.377-4.284,8.042-7.706,10.992c-3.423,2.951-6.951,5.523-10.568,7.71c-3.613,2.187-8.233,3.949-13.846,5.28   c-5.612,1.333-10.513,2.38-14.698,3.14c-4.188,0.762-9.421,1.287-15.703,1.571c-6.283,0.284-11.043,0.478-14.277,0.572   c-3.237,0.094-7.661,0.094-13.278,0c-5.618-0.094-8.897-0.144-9.851-0.144v-87.65H198.938z M318.998,316.331   c-1.813,4.38-4.141,8.189-6.994,11.427c-2.858,3.23-6.619,6.088-11.28,8.559c-4.66,2.478-9.185,4.473-13.559,5.996   c-4.38,1.529-9.664,2.854-15.844,4c-6.194,1.143-11.615,1.947-16.283,2.426c-4.661,0.477-10.226,0.856-16.7,1.144   c-6.469,0.28-11.516,0.425-15.131,0.425c-3.617,0-8.186-0.052-13.706-0.145c-5.523-0.089-9.041-0.137-10.565-0.137v-96.505   c1.521,0,6.042-0.093,13.562-0.287c7.521-0.192,13.656-0.281,18.415-0.281c4.758,0,11.327,0.281,19.705,0.856   c8.37,0.567,15.413,1.42,21.128,2.562c5.708,1.144,11.937,2.902,18.699,5.284c6.755,2.378,12.23,5.28,16.419,8.706   c4.188,3.432,7.707,7.803,10.561,13.134c2.861,5.328,4.288,11.42,4.288,18.274C321.712,307.104,320.809,311.95,318.998,316.331z" fill="#FFFFFF" />
                                </svg>
                                @endif
                                <h4>{{$cointypes->symbol}}</h4>
                                <p><span>Get {{ico()}} Coins using {{$cointypes->symbol}}</span></p>
                                <h6><br></h6>

                               
                            </label>
                        </div>
                        @endforeach

                       
                    </div>
                    <ul class="list-inline">
                        <li class="pull-right">
                            <button type="button" disabled="disabled" class="btn btn-primary step-1 next-step">continue</button>
                        </li>
                    </ul>
                </div>
                <div class="tab-pane" role="tabpanel" id="step2">
                    <div class="step2_contents">
                        <h4>
                            <svg id="bitcoin" class="BTC symbol" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 475.074 475.074" style="enable-background:new 0 0 475.074 475.074;" xml:space="preserve">
                                <path d="M395.655,249.236c-11.037-14.272-27.692-24.075-49.964-29.403c28.362-14.467,40.826-39.021,37.404-73.666   c-1.144-12.563-4.616-23.451-10.424-32.68c-5.812-9.231-13.655-16.652-23.559-22.266c-9.896-5.621-20.659-9.9-32.264-12.85   c-11.608-2.95-24.935-5.092-39.972-6.423V0h-43.964v69.949c-7.613,0-19.223,0.19-34.829,0.571V0h-43.97v71.948   c-6.283,0.191-15.513,0.288-27.694,0.288l-60.526-0.288v46.824h31.689c14.466,0,22.936,6.473,25.41,19.414v81.942   c1.906,0,3.427,0.098,4.57,0.288h-4.57v114.769c-1.521,9.705-7.04,14.562-16.558,14.562H74.747l-8.852,52.249h57.102   c3.617,0,8.848,0.048,15.703,0.134c6.851,0.096,11.988,0.144,15.415,0.144v72.803h43.977v-71.947   c7.992,0.195,19.602,0.288,34.829,0.288v71.659h43.965v-72.803c15.611-0.76,29.457-2.18,41.538-4.281   c12.087-2.101,23.653-5.379,34.69-9.855c11.036-4.47,20.266-10.041,27.688-16.703c7.426-6.656,13.559-15.13,18.421-25.41   c4.846-10.28,7.943-22.176,9.271-35.693C410.979,283.882,406.694,263.514,395.655,249.236z M198.938,121.904   c1.333,0,5.092-0.048,11.278-0.144c6.189-0.098,11.326-0.192,15.418-0.288c4.093-0.094,9.613,0.144,16.563,0.715   c6.947,0.571,12.799,1.334,17.556,2.284s9.996,2.521,15.701,4.71c5.715,2.187,10.28,4.853,13.702,7.993   c3.429,3.14,6.331,7.139,8.706,11.993c2.382,4.853,3.572,10.42,3.572,16.7c0,5.33-0.855,10.185-2.566,14.565   c-1.708,4.377-4.284,8.042-7.706,10.992c-3.423,2.951-6.951,5.523-10.568,7.71c-3.613,2.187-8.233,3.949-13.846,5.28   c-5.612,1.333-10.513,2.38-14.698,3.14c-4.188,0.762-9.421,1.287-15.703,1.571c-6.283,0.284-11.043,0.478-14.277,0.572   c-3.237,0.094-7.661,0.094-13.278,0c-5.618-0.094-8.897-0.144-9.851-0.144v-87.65H198.938z M318.998,316.331   c-1.813,4.38-4.141,8.189-6.994,11.427c-2.858,3.23-6.619,6.088-11.28,8.559c-4.66,2.478-9.185,4.473-13.559,5.996   c-4.38,1.529-9.664,2.854-15.844,4c-6.194,1.143-11.615,1.947-16.283,2.426c-4.661,0.477-10.226,0.856-16.7,1.144   c-6.469,0.28-11.516,0.425-15.131,0.425c-3.617,0-8.186-0.052-13.706-0.145c-5.523-0.089-9.041-0.137-10.565-0.137v-96.505   c1.521,0,6.042-0.093,13.562-0.287c7.521-0.192,13.656-0.281,18.415-0.281c4.758,0,11.327,0.281,19.705,0.856   c8.37,0.567,15.413,1.42,21.128,2.562c5.708,1.144,11.937,2.902,18.699,5.284c6.755,2.378,12.23,5.28,16.419,8.706   c4.188,3.432,7.707,7.803,10.561,13.134c2.861,5.328,4.288,11.42,4.288,18.274C321.712,307.104,320.809,311.95,318.998,316.331z" fill="#FFFFFF"/>
                            </svg>
                            <svg id="litecoin" class="ETH symbol" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="256px" height="417px" viewBox="0 0 256 417" version="1.1" preserveAspectRatio="xMidYMid">
                                <g>
                                    <polygon points="127.9611 0 125.1661 9.5 125.1661 285.168 127.9611 287.958 255.9231 212.32" />
                                    <polygon points="127.962 0 0 212.32 127.962 287.959 127.962 154.158" />
                                    <polygon points="127.9611 312.1866 126.3861 314.1066 126.3861 412.3056 127.9611 416.9066 255.9991 236.5866" />
                                    <polygon points="127.962 416.9052 127.962 312.1852 0 236.5852" />
                                    <polygon points="127.9611 287.9577 255.9211 212.3207 127.9611 154.1587" />
                                    <polygon points="0.0009 212.3208 127.9609 287.9578 127.9609 154.1588" />
                                </g>
                            </svg>
                            <svg id="xrp" class="XRP symbol" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 196.22 212.82">
                                <path class="cls-1" d="M431,153.4c-7.52-4.34-16-5.6-24.39-5.9-7-.25-17.55-4.76-17.55-17.57,0-9.54,7.74-17.23,17.55-17.57,8.39-.29,16.86-1.55,24.39-5.9A44.45,44.45,0,1,0,364.31,68c0,8.61,3.06,16.54,7,23.89,3.29,6.18,4.95,17.66-6.32,24.16-8.39,4.84-18.85,1.78-24.08-6.59-4.42-7.07-9.75-13.69-17.21-18a44.45,44.45,0,1,0,0,77c7.46-4.31,12.79-10.92,17.21-18,3.62-5.8,12.67-13.19,24.08-6.6,8.37,4.84,11,15.44,6.32,24.17-3.91,7.35-7,15.27-7,23.88A44.45,44.45,0,1,0,431,153.4Z" transform="translate(-256.99 -23.53)"/>
                            </svg>

                            <span><span class="payment"></span> Payment (1 {{ico()}} = {{ Setting::get('coin_price') }} USD)</span>
                        </h4>
                        <ul class="calculation_ul">
                            <li>
                                <div class="form-group">

                                    <label>{{ico()}} quantity to buy</label>

                                    <input id="ico" type="number" step="any" name="quantity" placeholder="Min. 100 {{ico()}}" min="100">
                                </div>
                            </li>
                            <li class="hidden-xs">
                                <span><i class="fa fa-arrow-circle-right" style="font-size:48px;color:#9aca3c"></i></span>
                            </li>
                            <li>
                                <div class="form-group">
                                    <label>Equivalent  <span class="payment"></span></label>
                                    <input class="price" type="number" step="any" name="">
                                </div>
                            </li>
                        </ul>
                        <ul class="calculation_result">
                            <li>
                                <p><input id="promocode" type="hidden" name="promocode" placeholder="Promocode"><span class="promo-txt no-promo"></span></p>
                            </li>
                            <li>
                                <p>Final USD = <span class="">&nbsp;USD</span> <span class="priceusd">0.00</span></p>
                                <!-- <input type="hidden" name="" id="priceusd" /> -->
                            </li>
                            <li>
                                <p>Final price = <span class="payment"></span> <span class="price">0.00</span></p>
                            </li>
                            @if(count($bonuses)>0)
                            <li>
                                <p>{{$bonuses[0]->percentage}}% Discount  <span>&nbsp;{{ico()}}</span><span id="bonus">  0.00</span></p>
                                <input type="hidden" id="bonus_point" name="bonus_point" value="{{$bonuses[0]->percentage}}" />
                            </li>
                            @endif
                            <li>
                                <p class="final_bdx">Final {{ico()}} = <span class="final_bdx">&nbsp;{{ico()}}</span> <span class="final_bdx" id="total">0.00  </span></p>
                            </li>
                        </ul>
                    </div>
                    <ul class="list-inline">
                        <li>
                            <button type="button" class="btn btn-default prev-step">Back to payment options</button>
                        </li>
                        <li class="pull-right">
                            <button type="button" disabled="disabled" class="btn btn-primary step-2 next-step">next step</button>
                        </li>
                    </ul>
                </div>

                <div class="tab-pane" role="tabpanel" id="step3">
                        <div class="eth-address-seciton">
                            <h4>Your ETH wallet</h4>
                            <p><strong>To receive {{ico()}} you'll need an ERC20-compliant ETH wallet. If you don't have one, create your ETH wallet for free at MyEtherWallet.com.</strong></p>
                            <div class="form-control">
                                <input type="text" placeholder="Ethereum Address" name="coin_address" value="{{ Auth::user()->coin_address }}" id="address">
                                <label style="color:red;display: block;text-align: center;" class="error_address"></label>
                            </div>
                            <p>Don't use your exchange wallets for buying {{ico()}}. Use personal wallet only! Your ETH address must start with "0x", eg.: "0x80d29f34570225bd21fe7c79c53425b3a4c71e71".</p>
                        </div>
                        <ul class="list-inline">
                            <li>
                                <button type="button" class="btn btn-default prev-step">Back to payment options</button>
                            </li>
                            <li class="pull-right">
                                <button type="button" @if(!Auth::user()->coin_address) disabled="disabled" @endif class="btn btn-primary step-3 btn-info-full next-step">PROCEED ORDER</button>
                            </li>
                        </ul>
                    </div>

                <div class="tab-pane" role="tabpanel" id="step4">
                    <div class="tab-top">
                        <h4><span class="payment"></span> Funds Only!</h4>
                        <p>Send only <span class="payment"></span> to this address. The coins will be <span> credited after we get confirmation from the network.</span></p>
                       <!--  <p><span>Please note</span> that the address for this funding is unique and can only be used once.</p> -->
                        <p><span>Exact {{ico()}} amount will be calculated at time of transaction.</p> 
                    </div>

                    <div>
                        <h6  id="pay">
                            <i class="fa fa-circle-o-notch fa-spin" style=""></i>
                            <span style="font-weight: 600;">Awaiting Payment, Please don't close or refresh this window.</span>
                        </h6>
                        <h6  id="payshow">
                            <i class="fa fa-circle-o-notch fa-spin"></i>
                            Payment received.
                        </h6>
                    </div>

                    <div class="QR-Code">
                        <p class="ico_launch"></p>
                        
                        <div id="clockdiv">
                            <div style="display: none;">
                                <span class="days"></span>
                                <div class="smalltext">Days</div>
                            </div>
                            <div style="display: none;">
                                <span class="hours"></span>
                                <div class="smalltext">Hours</div>
                            </div>
                            <div>
                                <span class="minutes"></span>
                                <div class="smalltext">Minutes</div>
                            </div>
                            <div>
                                <span class="seconds"></span>
                                <div class="smalltext">Seconds</div>
                            </div>
                        </div>

                        <p>Send <span class="amount price">0.0000000</span><strong> <span class="payment"></span></strong> to this address:</p> 

                        @foreach($cointype as $cointypes)
                            <div class="{{$cointypes->symbol}} qr-code" style="display: none;">
                                <!-- <img class="img-responsive" src="{{ img($cointypes->qr_code) }}" width="200"> -->

                                @if($cointypes->symbol == "BTC")
                                <?php $address = $btc_addr = Auth::user()->btc_address; ?>
                                <div id="qrcode-{{$cointypes->symbol}}" class="QRImage img-responsive" ins="{{Auth::user()->btc_address}}"></div>
                                @elseif($cointypes->symbol == "BCH")
                                <?php $address = $bch_addr = $cointypes->address; ?>
                                <div id="qrcode-{{$cointypes->symbol}}" class="QRImage img-responsive" ins="{{Auth::user()->bch_address}}"></div>
                                @elseif($cointypes->symbol == "ETH")
                                <?php $address = $eth_addr = Auth::user()->eth_address; ?>
                                <div id="qrcode-{{$cointypes->symbol}}" class="QRImage img-responsive" ins="{{Auth::user()->eth_address}}"></div>
                                @else
                                <?php $address = $cointypes->address; ?>
                                <div id="qrcode-{{$cointypes->symbol}}" class="QRImage img-responsive" ins="{{$cointypes->address}}"></div>
                                @endif


                                <div class="input-group mt-30">
                                    <label class="label">Address</label>
                                    <input type="text" placeholder="{{$cointypes->address}} Address" value="{{$address}}" id="address-{{$cointypes->address}}">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-theme" onclick="myFunction{{$cointypes->address}}()" >copy to clipboard</button>
                                    </span>
                                    
                                </div>
                                <div class="my_alert" style="display: none;">
                                    Please note that the address is unique and it can used only for one transaction.
                                </div>

                                @if($cointypes->symbol == "XRP")
                                <div class="input-group mt-30">
                                    <label class="label">Destination Tag</label>
                                    <input type="text" value="<?php echo $dest_tag = Auth::user()->dest_tag; ?>" id="address-{{$cointypes->symbol}}">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-theme" onclick="myFunction{{$cointypes->symbol}}()" >copy to clipboard</button>
                                    </span>
                                </div>
                                @endif

                                <script>
                                    function myFunction{{$cointypes->symbol}}() {
                                        var copyText = document.getElementById("address-{{$cointypes->symbol}}");
                                        copyText.select();
                                        document.execCommand("Copy");
                                        $('.my_alert').show();
                                    }
                                </script>
                                <script>
                                    function myFunction{{$cointypes->address}}() {
                                        var copyText = document.getElementById("address-{{$cointypes->address}}");
                                        copyText.select();
                                        document.execCommand("Copy");
                                        $('.my_alert').show();
                                    }
                                </script>
                            </div>
                            @if($cointypes->symbol == "XRP")
                                <?php 
                                $ripple_addr = $cointypes->address;
                                ?>
                                <br>
                                <!-- <h6  id="pay">
                                    <i class="fa fa-circle-o-notch fa-spin"></i>
                                    Awaiting Payment, Please don't close or refresh this window.
                                </h6>
                                <h6  id="payshow">
                                    <i class="fa fa-circle-o-notch fa-spin"></i>
                                    Payment received.
                                </h6> -->
                            @endif
                        @endforeach

                        <form method="post" action="{{url('transaction')}}" id="transaction_form">
                            {{csrf_field()}}
                            <!-- <label>Transaction ID</label> -->

                            <input type="hidden"  id="amount_ctc"  name="amount_ctc" required="">
                            <input type="hidden" id="amount_new"  name="amount_new" required="">
                            <input type="hidden" id="promo_code"  name="promo_code" required="">
                            <input type="hidden" id="cointype"  name="cointype" required="">
                            <input type="hidden" id="coin_address"  name="coin_address" required="">
                            <input type="hidden" id="f"  name="referal_code" value="0">
                            <input type="hidden" id="txn_address"  name="address">
                            <input type="hidden" name="priceusd" id="priceusd" />
                            <input type="hidden" name="selectedcoin" id="selectedcoin" />
                            <input type="text" placeholder="Transaction Id" name="tranx_id" id="tranx_id">

                            @if(count($bonuses)>0)
                            <input type="hidden"  name="bonus_point" value="{{$bonuses[0]->percentage}}" />
                            @else
                            <input type="hidden"  name="bonus_point" value="0" />
                            @endif



                            <!-- <div class="input-group">
                            <input type="text" placeholder="Transaction Id" name="tranx_id" id="tranx_id"> -->
                            <!--  <span class="">  -->
                                <!-- <button type="button" id="btn-token-promocode-add-new" disabled="" class="btn btn-theme btn-green"> Proceed </button> -->


                                <!-- <button type="button" id="btn-token-promocode-add-new" disabled="" class="step-3 btn btn-theme btn-green"> Proceed </button> -->

                                <!-- <button type="button" id="btn-token-promocode-add-new" class="btn btn-theme btn-green" data-toggle="modal" data-target="#myModal" disabled="" data-backdrop="static" data-keyboard="false">Proceed</button> -->

                               <!--  <button type="button" id="btn-token-promocode-add-new" class="btn btn-theme btn-green" data-toggle="modal" data-target="#myModal" >Proceed</button> -->

                                <!-- <div id="coin_type_btn_submit">
                                    
                                </div> -->
                                
                            <!-- </span>
                            </div> -->
                            <label style="color:red;display: block;" class="error_tranx_id"></label>
                        </form>
                        <!-- <p class="received-amount">Amount Received: <span class="amount price">0.000000</span><strong> <span class="payment"></span></strong></p> -->
                    </div>
                    <ul class="list-inline">
                        <li>
                            <button type="button" class="btn btn-default prev-step">Back to payment options</button>
                        </li>
                        <li class="pull-right">

                            <!-- <button disabled="disabled" class="btn btn-primary step-4 btn-info-full next-step" onclick="window.location.href='{{ url('/transaction') }}'">CHECK MY TRANSACTIONS</button> -->

                            <!-- <a href="{{ url('/transaction') }}" disabled="disabled" class="btn btn-primary step-4 btn-info-full next-step" >CHECK MY TRANSACTIONS</a> -->
                        </li>
                    </ul>
                </div>

                <div class="clearfix"></div>
                </div>
                </div>
                <div class="col-md-3 side_wizard wizard" style="padding: 0px; background: #f5f5f5; height: auto;">
                    <div class="page">
                      <div class="page__demo">
                        <div class="main-container page__container">
                          <div class="timeline">
                            <div class="timeline__group">
                              <span class="timeline__year">Steps</span>
                              <div class="timeline__box">
                                <div class="timeline__date">
                                  <span class="timeline__day">1</span>                              
                              </div>
                                  <div class="timeline__post">
                                      <div class="timeline__content">
                                        <p>Choose a payment option like BTC, ETH or XRP to buy your {{ico()}} coin.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="timeline__box">
                                <div class="timeline__date">
                                  <span class="timeline__day">2</span>
                              </div>
                              <div class="timeline__post">
                                  <div class="timeline__content">
                                    <p>Specify your value of {{ico()}} to buy through selected payment type.</p>
                                </div>
                            </div>
                        </div>
                        <div class="timeline__box">
                                <div class="timeline__date">
                                  <span class="timeline__day">3</span>
                              </div>
                              <div class="timeline__post">
                                  <div class="timeline__content">
                                    <p>Scan the QR-code or copy the address given below to buy {{ico()}} through selected cryptocurrency.  </p>
                                </div>
                            </div>
                        </div>
                        <div class="timeline__box">
                                <div class="timeline__date">
                                  <span class="timeline__day">4</span>
                              </div>
                              <div class="timeline__post">
                                  <div class="timeline__content">
                                    <p>Please note that the address of BTC (or) ETH is unique and it can used only for one transaction.</p>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

</div>

</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <p style="font-size: 1.5em; color: #555;text-align: center;">Have you paid your payment, if not please use the address given and make your payment and click proceed again. </p>
        <br>
        <p style="font-size: 1.5em; color: #555;text-align: center;">Once the payment have confirmed, your {{ico()}} Coin will be credited to your wallet...</p>
        
        <!--<p style="font-size: 1.5em; color: #555;text-align: center;">Your payment has been credited successfully, Once the payment have confirmed, your BDX Coin will be credited to your wallet... </p> -->
      </div>
      <div class="modal-footer">
        <button type="button" id="btn-token-promocode-add-new" class="step-3 btn btn-theme btn-green">I have paid</button>

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection

@section('scripts')

<script>

    /*function mycoinselect(id){
        $('#selectedcoin').val(id);


        if(id == "BTC"){
            $('#coin_type_btn_submit').html('<button type="button" id="btn-token-promocode-add-new" class="btn btn-theme btn-green" data-toggle="modal" data-target="#myModal" disabled="" >Proceed</button>');
        }else{
            $('#coin_type_btn_submit').html('<button type="button" id="btn-token-promocode-add-new" disabled="" class="step-3  btn btn-theme btn-green"> Proceed </button>');
        }
        
    }
    */

    $(document).ready(function() {

        var payment, promo, ico, price;

        $('.pay-method input[type=radio]').change(function(){
           
            if(payment = $(this).val()) { //XRP Disable
                
                $('.step-1').prop('disabled',false);
                //$('.payment').html(payment);
                $('.step2_contents > h4 > span > span.payment').html(payment);
                $('.qr-code, .symbol').hide();
                $('.'+payment).show();
                $('#ico').trigger('change');
            }
        });

        $("#ico, #promocode, .price").change(function(){
            promo = $('#promocode').val();
            ico = $('#ico').val();
            price = $('.price').val();
            if(ico || price) {

                id = $(this).attr('id');
                $.get("{{url('/coinstatus')}}?promo="+promo+"&type="+payment, function(data, status) {
                    if(data.message == 'OK') {
                        if(payment == 'ETH') {
                            var live_price = data.result.ethusd;
                        } else {
                            var live_price = data.last;
                        }

                        if(id == 'ico') {
                            var bitcoinusd = (parseFloat(<?php echo Setting::get('coin_price') ;?>) * ico).toFixed(8);
                            $(".priceusd").html(parseFloat(bitcoinusd));
                            $("#priceusd").val(parseFloat(bitcoinusd));
                            
                            var bitcoin = (parseFloat(<?php echo Setting::get('coin_price') ;?>) * ico / live_price).toFixed(8);
                            $(".price").val(parseFloat(bitcoin));
                            $(".price").html(parseFloat(bitcoin));
                        } else {
                            ico = parseInt(live_price * price / <?php echo Setting::get('coin_price') ;?>)
                            $("#ico").val(ico);
                            $(".price").html(parseFloat(price));

                            var bitcoinusd = (parseFloat(<?php echo Setting::get('coin_price') ;?>) * ico).toFixed(8);
                            $(".priceusd").html(parseFloat(bitcoinusd));
                            $("#priceusd").val(parseFloat(bitcoinusd));
                        }

                        var bonus_point = parseFloat($('#bonus_point').val());
                        var bonus = parseFloat(ico)*(bonus_point/100);
                        $("#bonus").html(parseFloat(bonus));
                        if(data.promo_percent) {
                            var promo_percent = parseFloat(ico)*(data.promo_percent/100);
                            $(".promo-txt").removeClass('no-promo');
                            $(".promo-txt").html('Promocode Applied');
                        } else {
                            var promo_percent = 0;
                        }
                        if(bonus_point) {
                            var total = parseFloat(ico)+parseFloat(bonus)+parseFloat(promo_percent);
                        } else {
                            var total = parseFloat(ico)+parseFloat(promo_percent);
                        }
                        $("#total").html(parseFloat(total));
                        
                        if(ico>=100){
                            $('.step-2').prop('disabled',false);
                        }else{
                            $('.step-2').prop('disabled',true);
                            alert("Amount of {{ico()}} to buy should be greater than or equal to 100");
                        }

                    } else {
                        if(data.error == 'invalid_promo'){
                            $(".promo-txt").html('Invalid Promocode');
                        }else if(data.error == 'promo_expired'){
                            $(".promo-txt").html('Promocode Expired');
                        }else if(data.error == 'promo_used'){
                            $(".promo-txt").html('Promocode already Used');
                        }
                        $(".promo-txt").addClass('no-promo');
                        $('#promocode').val('');
                        $('#ico').trigger('change');
                    }
                });
                
            }
        });

        $(".step-1").click(function(){
            $('.wiz1').hide();
            $('.wiz2').show();            
        });        

        $(".step-2").click(function(){
            var n=0;            
            $('#payshow').hide();
            if(payment == 'XRP'){
                $('#tranx_id').hide();
                checkTransaction();
            } else if(payment == 'BTC'){
                $('#tranx_id').hide();
                checkBTCTransaction();
            } else if(payment == 'ETH'){
                $('#tranx_id').hide();
                checkETHTransaction();
            } else {
                n=1;
                $('#tranx_id').show();
                $('#transaction_form button').attr('disabled', false);
                $('.step-3').attr('disabled', false);
            }

            $('.wiz2').hide();
            $('.wiz3').show();

            $('.QRImage').html('');
            
            var code = 'bitcoin:'+$('#qrcode-BTC').attr('ins')+'?amount='+$('.price').val();

            $('#qrcode-BTC').qrcode(code);

            var code = $('#qrcode-ETH').attr('ins');

            $('#qrcode-ETH').qrcode(code);

            var code = $('#qrcode-XRP').attr('ins');

            $('#qrcode-XRP').qrcode(code);


            if(n==0){

                $.ajax({
                  url: "{{url('/paymentsecondstep')}}",
                  type: "GET",
                  data:{'usd':$('#priceusd').val(),'price':$('.price').val(),'cointype':payment,'amount_ctc':$('#ico').val(),'ico':'{{ico()}}'}
              }).done(function(response){

              }).fail(function(jqXhr,status){

              });

          }

        });

        $('#address').change(function(){
            $('.error_address').html('');


            
            if(!$(this).val()) {
                $('.step-3').prop('disabled',true);
            } else {
                $.ajax({
                  url: "https://api.etherscan.io/api",
                  type: "GET",
                  data:{'module':'account','action':'balance','address':$(this).val()}
                }).done(function(response){
                    if(response.status == 0) {
                        $('.error_address').html('Invalid Address!');
                        $('.step-3').prop('disabled',true);
                    } else {
                        $('.step-3').prop('disabled',false);
                        $('#coin_address').val($('#address').val());
                    }
                });
            }
        
        
        });
        $(".step-3").click(function(){
            if(payment == 'XRP'){
                $('#tranx_id').hide();
               
                checkTransaction();
            }
        });

        //$('.proceed').on('click',function(){
        $('.step-4').on('click',function(){
            $('#myModal').modal('hide');
            myfunclaststep();            
        });

        // Last Step Functionality
        function myfunclaststep(){
            if($('#tranx_id').val()){
                var myVar;
                $.ajax({
                  url: "{{url('checkTransaction')}}",
                  type: "GET",
                  data:{'tranx_id':$('#tranx_id').val(),'amount':$('.price').val(),'amount_ctc':$('#ico').val(),'cointype':payment}
                  }).done(function(response){
                    if(response.success == 'Ok'){
                        $(".error_tranx_id").html('');
                        $('#step-3').prop('disabled',false);
                        $('#amount_ctc').val($('#ico').val());
                        if(!$('#amount_new').val()) {
                            $('#amount_new').val($('.price').val());
                        }
                        $('#promo_code').val($('#promocode').val());
                        $('#cointype').val(payment);
                        $('#coin_address').val($('#address').val())
                        $('#txn_address').val($('#address').val())
                        $('#transaction_form').submit();  // Save transaction
                        $('#payshow').show();
                    }else if(response.error == 'id_not_valid'){
                        error =1;
                        $('#step-3').prop('disabled',true);
                        $('#msg').html('');
                        $(".error_tranx_id").html('Invalid Transaction ID');
                    }else if(response.error == 'price_not_match'){
                        error =1;
                        $('#step-3').prop('disabled',true);
                        $('#msg').html('');
                        $(".error_tranx_id").html('Price does not match');
                    }else if(response.error == 'address_not_match'){
                        error =1;
                        $('#step-3').prop('disabled',true);
                        $('#msg').html('');
                        $(".error_tranx_id").html('Deposit Address does not match');
                    } else{
                        $('#loader').show();
                        $('#step-3').prop('disabled',true);
                        $('#msg').html('Please wait until your Transaction is Processing...');

                    }

                }).fail(function(jqXhr,status){
                    if(jqXhr.status === 422) {
                        error = 1;
                        $(".error_tranx_id").html('');
                        $(".error_tranx_id ").show();
                        var errors = jqXhr.responseJSON;
                        console.log(errors);
                        $.each( errors , function( key, value ) {
                            $(".error_tranx_id").html(value);
                        });
                    }
                    $('#step-3').prop('disabled',true);
                });
            }else{
                $('.error_tranx_id').html('This field is required');
            }
        }

        // BTC Function
        var error_btc = 0;
        function checkBTCTransaction(){
            var amount_btc = $('#price').val();
            var add = '{{$btc_addr}}';
            $.ajax({
              url: "{{url('checkbtctran')}}",
              type: "GET",
              data:{'tx_id': add ,'btc': amount_btc}
            })
            .done(function(response){
                console.log(response);
                // alert(response);
                var transa =1;
                var amount_btc = $('.price').val();
                console.log(response);


                //if(parseFloat(response.txApperances) > 0){
                    transa =0; 
                    if(response.amount) {
                        var total = response.amount;
                    }
                        // } else {
                        //     var total = response.unconfirmedBalanceSat;
                        // }
                        // console.log(value.total/1000000000000000000 +'---'+ amount_btc+'--'+parseFloat(amount_btc - (value.total/1000000000000000000)));
                        if(total) {

                            // $.each(response.transactions ,function(index,value){
                               // console.log(response.transactions[0]);
                               var ids = response.txid;
                               var amount_new = response.amount;
                               var address = response.address;
                               // console.log(amount_btc);
                               // if(parseFloat(amount_btc - (total/100000000)) < 0.0005) {
                                $.ajax({
                                  url: "{{url('checkTrans')}}",
                                  type: "GET",
                                  data:{'tx_id': ids}
                                })
                                .done(function(response){
                                    console.log(response);
                                    if(response == '1') {
                                        setTimeout(function() { checkBTCTransaction(); }, 5000);
                                    } else {
                                        $('#tranx_id').val(ids);
                                        $('#amount_new').val(amount_new);
                                        $('#address').val(address);

                                        $('.fa').remove();
                                        $('#pay').hide();
                                        
                                        $('#payshow').show();

                                        $("#waiting-msg-Bitcoin").html('<i class="fa fa-check"></i> @lang('user.common.success')');
                                        $('#btn-token-promocode-add-new').prop('disabled', false);
                                        //$('#myModal').modal('show');

                                        myfunclaststep();
                                    }
                                });

                                // }else{
                                //   setTimeout(function() { checkBTCTransaction(); }, 5000);
                                // }
                            // });
                        } else {
                            setTimeout(function() { checkBTCTransaction(); }, 5000);
                        }
                        if(transa==1){
                          setTimeout(function() { checkBTCTransaction(); }, 5000);
                        }

                // }
                //else{
                    //setTimeout(function() { checkBTCTransaction(); }, 5000);
                // }

            })
            .error(function(jqXhr,status){
                if(jqXhr.status === 422) {
                    error_eth =1;
                    $("#waiting-msg-Bitcoin").html('');
                    var errors = jqXhr.responseJSON;
                    console.log(errors);
                    $.each( errors , function( key, value ) { 
                        $("#waiting-msg-Bitcoin").html(value);
                    }); 
                }
            })
        }
        
        // Etherium Function
        var error_eth = 0;
        function checkETHTransaction(){
            $.ajax({
              url: "https://api.etherscan.io/api?module=account&action=txlist&address={{$eth_addr}}&page=1&offset=1&sort=desc",
              type: "GET",
                  //data:{'payment_id':$('#transaction_id_'+type).val(),'amount':amount}
            })
            .done(function(response){
                console.log(response);
                var transa =1;
                var amount_eth = $('#price').val();
                if((response.result).length > 0){
                    console.log(amount_eth);
                    $.each(response.result ,function(index,value){

                        if(value.to == {{$eth_addr}}) {
                         transa =0;
                               // console.log(value.value/1000000000000000000 +'---'+ amount_eth+'--'+parseFloat(amount_eth - (value.value/1000000000000000000)));
                               var ids = value.hash;
                               // if(parseFloat(amount_eth - (value.value/1000000000000000000)) < 0.005) {
                                  $.ajax({
                                      url: "{{url('checkTrans')}}",
                                      type: "GET",
                                      data:{'tx_id': ids}
                                  }).done(function(response){
                                      if(response == '1') {
                                        setTimeout(function() { checkETHTransaction(); }, 5000);
                                    } else {
                                        $('#tranx_id').val(value.hash);
                                        $('#amount_new').val(value.value/1000000000000000000);
                                        $('#address').val(value.to);
                                        $('.fa').remove();
                                        $('#pay').hide();  
                                        $('#payshow').show();
                                        $("#waiting-msg-Ethereum").html('<i class="fa fa-check"></i> @lang('user.common.success')');
                                        $('#btn-token-promocode-add-new').prop('disabled', false);
                                        //$('#myModal').modal('show');

                                        myfunclaststep();
                                    }
                                });
                                // }else{
                                //   setTimeout(function() { checkETHTransaction(); }, 5000);
                                // }
                            }
                        });

                    if(transa==1){
                      setTimeout(function() { checkETHTransaction(); }, 5000);
                  }

              }
              else{
                  setTimeout(function() { checkETHTransaction(); }, 5000);
              }

            })
            .error(function(jqXhr,status){
                if(jqXhr.status === 422) {
                    error_eth =1;
                    $("#waiting-msg-Ethereum").html('');
                    var errors = jqXhr.responseJSON;
                    console.log(errors);
                    $.each( errors , function( key, value ) { 
                        $("#waiting-msg-Ethereum").html(value);
                    }); 
                }
            })
        }

        // Ripple Function
        var error = 0;
        function checkTransaction(){
            $.ajax({
                url: "https://data.ripple.com/v2/accounts/{{$ripple_addr}}/transactions?type=Payment&result=tesSUCCESS&limit=15",
                type: "GET",
                    //data:{'payment_id':$('#transaction_id_'+type).val(),'amount':amount}
            })
            .done(function(response){
                var transa =1;
                var amount_ripple = $('#price').val();
                if((response.transactions).length > 0){
                    console.log(response)
                    $.each(response.transactions ,function(index,value){
                        console.log(value.tx.DestinationTag);
                        if(value.tx.DestinationTag == {{$dest_tag}}){
                         transa =0;
                           // console.log(value.tx.Amount/1000000 +'---'+ amount_ripple+'--'+parseFloat(amount_ripple - (value.tx.Amount/1000000)));
                           // if(parseFloat(amount_ripple - (value.tx.Amount/1000000)) < 2){
                            $('#tranx_id').val(value.hash);
                            $('#amount_new').val(value.tx.Amount/1000000);
                            $('#address').val(value.tx.DestinationTag);
                            $('.fa').remove();
                            $('#pay').hide();  
                            $('#payshow').show();
                            $("#waiting-msg-Ethereum").html('<i class="fa fa-check"></i> @lang('user.common.success')');
                            $('#btn-token-promocode-add-new').prop('disabled', false);
                            //$('#myModal').modal('show');

                            myfunclaststep();

                            // }else{
                            //     $("#waiting-msg-Ripple").html('<i class="fa fa-close"></i> Paid amount is less than the order amount. Please re-pay the exact amount.');
                            // }
                        }
                    });

                    if(transa==1){
                      setTimeout(function() { checkTransaction(); }, 5000);
                  }

              }
              else{
                  setTimeout(function() { checkTransaction(); }, 5000);
              }

            })
            .fail(function(jqXhr,status){
                if(jqXhr.status === 422) {
                    error =1;
                    $("#waiting-msg-Ripple").html('');
                    var errors = jqXhr.responseJSON;
                    console.log(errors);
                    $.each( errors , function( key, value ) { 
                        $("#waiting-msg-Ripple").html(value);
                    }); 
                }
            })
        }



        // Steps Function
        $(".c1").click(function(){
            $('.wiz1').show();
            $('.wiz2').hide();
            $('.wiz3').hide();
        });

        $(".c2").click(function(){
            $('.wiz1').hide();
            $('.wiz2').show();
            $('.wiz3').hide();
        });

        $(".c3").click(function(){
            $('.wiz1').hide();
            $('.wiz2').hide();
            $('.wiz3').show();
        });       

    });

</script>

<script type="text/javascript" src="{{asset('js/jquery.qrcode.js')}}"></script>
<script type="text/javascript" src="{{asset('js/qrcode.js')}}"></script>
<!-- <script type="text/javascript">
  $(document).ready(function() {
    @foreach($cointype as $cointypes)
        $('#qrcode-{{$cointypes->symbol}}').qrcode($('#qrcode-{{$cointypes->symbol}}').attr('ins'));
    @endforeach
  });
</script> -->
@endsection
@section('styles')
<style type="text/css">
    .pay-method label:hover .cmg-soon, .pay-method input:checked+label .cmg-soon{
        color: #fff;
    }


.promo-txt.no-promo {
    color: red !important;
}
.timeline{
  --uiTimelineMainColor: var(--timelineMainColor, #222);
  --uiTimelineSecondaryColor: var(--timelineSecondaryColor, #fff);

  position: relative;
  padding-top: 0;
  padding-bottom: 0;
}

.timeline:before{
  content: "";
  width: 4px;
  height: 100%;
  background-color: var(--uiTimelineMainColor);

  position: absolute;
  top: 0;
}

.timeline__group{
  position: relative;
}

.timeline__group:not(:first-of-type){
  margin-top: 4rem;
}

.timeline__year{
  padding: .5rem 1.5rem;
  color: var(--uiTimelineSecondaryColor);
  background-color: var(--uiTimelineMainColor);

  position: absolute;
  left: 0;
  top: 0;
  font-weight: 700;
}

@media screen and (min-width: 641px){
.timeline__date {
    top: 50%;
    margin-top: -27px;
    border-radius: 50px;
    min-width: auto;
    left: 12px;
}
}

.timeline__date {
    top: 50%;
    margin-top: -27px;
    border-radius: 50px !important;
    min-width: auto !important;
    left: 12px !important;
}

.timeline__box{
  position: relative;
}

.timeline__box:not(:last-of-type){
  margin-bottom: 30px;
}

.timeline__box:before{
  content: "";
  width: 100%;
  height: 2px;
  background-color: var(--uiTimelineMainColor);

  position: absolute;
  left: 0;
  z-index: -1;
}

.timeline__date{
  min-width: 65px;
  position: absolute;
  left: 0;

  box-sizing: border-box;
  padding: .5rem 1.5rem;
  text-align: center;

  background-color: var(--uiTimelineMainColor);
  color: var(--uiTimelineSecondaryColor);
}

.timeline__day{
  font-size: 2rem;
  font-weight: 700;
  display: block;
}

.timeline__month{
  display: block;
  font-size: .8em;
  text-transform: uppercase;
}

.timeline__post{
  padding: 1.5rem 2rem;
  border-radius: 2px;
  border-left: 3px solid var(--uiTimelineMainColor);
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, .12), 0 1px 2px 0 rgba(0, 0, 0, .24);
  background-color: var(--uiTimelineSecondaryColor);
}

@media screen and (min-width: 641px){

  .timeline:before{
    left: 30px;
}

.timeline__group{
    padding-top: 55px;
}

.timeline__box{
    padding-left: 80px;
}

.timeline__box:before{
    top: 50%;
    -webkit-transform: translateY(-50%);
    transform: translateY(-50%);  
}  

.timeline__date{
    top: 50%;
    margin-top: -27px;
}
}

@media screen and (max-width: 640px){

  .timeline:before{
    left: 0;
}

.timeline__group{
    padding-top: 40px;
}

.timeline__box{
    padding-left: 20px;
    padding-top: 70px;
}

.timeline__box:before{
    top: 90px;
}    

.timeline__date{
    top: 0;
}
}

.timeline{
  /*--timelineMainColor: #195260;*/
     --timelineMainColor: #318205;
  font-size: 16px;
}

@media screen and (min-width: 768px){

  html{
    font-size: 62.5%;
}
}

@media screen and (max-width: 767px){

  html{
    font-size: 55%;
}
}

/*
* demo page
*/

@media screen and (min-width: 768px){

  html{
    font-size: 62.5%;
}
}

@media screen and (max-width: 767px){

  html{
    font-size: 50%;
}
}


p{
  margin-top: 0;
  margin-bottom: 1.5rem;
  line-height: 1.5;
}

p:last-child{
  margin-bottom: 0;
}

.page{
  display: flex;
  flex-direction: column;
  justify-content: space-around;
}



.main-container{
  max-width: 960px;
  padding-left: 2rem;
  padding-right: 2rem;

  margin-left: auto;
  margin-right: auto;
}

.page__container{
  padding-top: 30px;
  padding-bottom: 30px;
  max-width: 800px;
}

.footer{
  padding-top: 1rem;
  padding-bottom: 1rem;
  text-align: center;  
  font-size: 1.4rem;
}

.footer__link{
  text-decoration: none;
  color: inherit;
}

@media screen and (min-width: 361px){

  .footer__container{
    display: flex;
    justify-content: space-between;
}
}

@media screen and (max-width: 360px){

  .melnik909{
    display: none;
} 
}
</style>
@endsection