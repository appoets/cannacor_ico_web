@extends('layouts.app')

@section('content')

    <div class="referral-section">
        <div class="container">
            <div class="transaction_balance">
                <!-- <h1 class="text-center">Give {{ Setting::get('referral_bonus') }}% Discount.</h1> -->
                <h4>Join our referral program and share with your friends! Get {{ Setting::get('referral_bonus') }}% {{ico()}} Coin from all your friend’s purchase!  It's a win-win for both! </h4>
                <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 referral-grid">
                    <h4>1</h4>
                    <p>Share your unique link and refer friends.</p>
                </div>
                <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 referral-grid">
                    <h4>2</h4>
                    <p>Gain {{ Setting::get('referral_bonus') }}% {{ico()}} coin your friends have bought.</p>
                </div>
                <!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 referral-grid">
                    <h4>3</h4>
                    <p>Your friends will also get {{ Setting::get('referral_bonus') }}% bonus for using your link.</p>
                </div> -->
                <div class="info-referral">
                    <p> Share the invitation link to your friends and others.  Become a Great Referrer for every Sign-up by  your invitation!  Get the exciting {{ Setting::get('referral_bonus') }}%  {{ico()}} coins for every purchase by your friends!!</p>
                </div>
                <form>
                    <div class="form-group">
                        <input type="text" id="refer-link" value="{{ url('register?referral='.Auth::user()->email_token) }}">
                        <button  type=button onclick="myFunction()">copy to clipboard</button>
                    </div>
                </form>
                <script>
                    function myFunction() {
                        var copyText = document.getElementById("refer-link");
                        copyText.select();
                        document.execCommand("Copy");
                    }
                </script>
                {{--<ul>
                    <li class="fb"><a href="https://www.facebook.com/dialog/feed?app_id=1400405506770805&display=popup&amp;caption={{Setting::get('referral_content')}}&link={{ url('register?referral='.Auth::user()->email_token) }}&redirect_uri={{ url('referral') }}" target="_blank"><i class="fa fa-facebook"></i> Share via Facebook</a></li>

                    <li class="tw"><a href="http://twitter.com/share?text={{ urlencode(Setting::get('referral_content')) }}&url={{ url('register?referral='.Auth::user()->email_token) }}" target="_blank"><i class="fa fa-twitter-square"></i> Share via Twitter</a></li>
                    
                    <li class="em"><a href="mailto:?subject=Setting::get('site_title')&body={{Setting::get('referral_content')}} {{ url('register?referral='.Auth::user()->email_token) }}"><i class="fa fa-envelope"></i> Share via Email</a></li>
                </ul>--}}
            </div>
            <div class="transaction_balance">
                <div class="section-title">
                    <h4>Referrals</h4>
                </div>
                @if(count($User))

                    <div class="table-responsive">
                    <table class="table" id="myTable">
                      <thead>
                        <tr>           
                          <th scope="col"><span>S.No </span></th>
                          <th scope="col"><span>Name</span></th>
                          <th scope="col"><span>Date</span></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($User as $key => $user)
                            <tr>
                              <td>{{ $key + 1 }}</td>
                              <td>{{$user->name}}</td>
                              <td>{{$user->created_at}}</td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                    </div>
                @else
                    <div class="referral_got_count">
                        <p>You have no referrals. Invite your friends to get bonuses.</p>
                    </div>
                @endif
            </div>

             <div class="transaction_balance">
            <div class="section-title">
                <h4>Referral Users Transactions</h4>
            </div>
            <div class="table-responsive">
            <table class="table" id="myTable1">
              <thead>
                <tr>           
                  <th scope="col"><span>Sl.No</span></th>
                  <th scope="col"><span>Date/Time</span></th>
                  <th scope="col"><span>Name</span></th>
                  <th scope="col"><span>Mail ID</span></th>

                 <!--  <th scope="col"><span>Transaction</span></th>
                  <th scope="col"><span>Payment</span></th> -->
                  <th scope="col"><span>Buy</span></th>
                  <th scope="col"><span>Referral</span></th>

                 <!--  <th scope="col"><span>{{ico()}} Price</span></th>
                  <th scope="col"><span>Price</span></th>
                  <th scope="col"><span>Status</span></th>
                  <th scope="col"><span>Date</span></th> -->
                </tr>
              </thead>
              <tbody>
                @forelse($Transactions as $index => $history)
                  <tr>
                      <td>{{ $index + 1 }}</td>
                      <td>{{ date('d M Y H:i:s', strtotime($history->created_at)) }}</td>
                      <td>{{ $history->user->name }}</td>
                      <td>{{ $history->user->email }}</td>
                     <!--  <td>
                          @if($history->payment_mode == 'BTC')
                              <a target="_blank" href="https://blockchain.info/tx/{{ $history->payment_id }}">
                          @elseif($history->payment_mode == 'ETH')
                              <a target="_blank" href="https://etherscan.io/tx/{{ $history->payment_id }}">
                          @elseif($history->payment_mode == 'XRP')
                              <a target="_blank" href="https://xrpcharts.ripple.com/#/transactions/{{ $history->payment_id }}">
                          @endif
                              {{substr($history->payment_id, 0, 8).'****'}}
                          </a>
                      </td> -->
                     <!--  <td>{{ $history->payment_mode }}</td> -->
                      <td>{{ $history->ico }}</td>
                      <td>{{ ($history->ico/100)*10 }}</td>
                     <!--  <td>{{ currency($history->ico_price) }}</td>
                      <td>{{ $history->price }}</td>
                      @if($history->status == "pending")
                      <td style="color: #fc8019;">{{ $history->status }}</td>
                      @elseif($history->status == "processing")
                      <td style="color: #CCCC00;">{{ $history->status }}</td>
                      @elseif($history->status == "success")
                      <td style="color: #008000;">{{ $history->status }}</td>
                      @elseif($history->status == "failed")
                      <td style="color: #FF0000;">{{ $history->status }}</td>
                      @endif
                      <td>{{ date('d M Y H:i:s', strtotime($history->created_at)) }}</td> -->

                  </tr>
                  @empty
                    <p>No record Found!</p>
                  @endforelse
              </tbody>
            </table>
            </div>
            <div class="text-center common-button">
               <!--  <button type="button" class="btn btn-primary btn-info-full next-step">PROCEED</button> -->
            </div>
        </div>

        </div>
    </div>
@endsection

@section('styles')
<style type="text/css">
@media (max-width: 991px) {
    #myTable thead {
      display: none;
    }
    #myTable td {
      word-break: none;
    }
    #myTable td:nth-of-type(1):before { content: "S.No" ; }
    #myTable td:nth-of-type(2):before { content: "Name"; }
    #myTable td:nth-of-type(3):before { content: "Date"; }

    #myTable td:first-child.dataTables_empty {
      text-align:  center;
      width:  100%;
    }

    #myTable td:first-child.dataTables_empty:before {
      display:  none;
    }

    #myTable td::before {
      width: 25%;
      display: inline-block;
    }
    #myTable td {
      padding: 10px !important;
      width: 100%;
      display: inline-block;
      text-align: left;
    }
    .transaction_balance table tbody tr th, .transaction_balance table tbody tr td {
      border: 1px solid #cacaca;
    }
    #myTable td:last-child {
      border-bottom: 0 !important;
    }
    #myTable tbody tr {
      margin: 20px 0;
      display: inline-block;
      width: 100%;
      border: 1px solid #cacaca;
  }
  .transaction_balance table tbody tr th, .transaction_balance table tbody tr td {
      border-bottom: 1px solid #cecece !important;
  }
}


@media (max-width: 991px) {
    #myTable1 thead {
      display: none;
    }
    #myTable1 td {
      word-break: none;
    }
    #myTable1 td:nth-of-type(1):before { content: "ID" ; }
    #myTable1 td:nth-of-type(2):before { content: "Transaction"; }
    #myTable1 td:nth-of-type(3):before { content: "Payment"; }
    #myTable1 td:nth-of-type(4):before { content: "BDX Quantity"; }
    #myTable1 td:nth-of-type(5):before { content: "BDX Price"; }
    #myTable1 td:nth-of-type(6):before { content: "Price"; }
    #myTable1 td:nth-of-type(7):before { content: "Status"; }
    #myTable1 td:nth-of-type(8):before { content: "Date"; }

    #myTable1 td:first-child.dataTables_empty {
      text-align:  center;
      width:  100%;
    }

    #myTable1 td:first-child.dataTables_empty:before {
      display:  none;
    }

    #myTable1 td::before {
      width: 25%;
      display: inline-block;
    }
    #myTable1 td {
      padding: 10px !important;
      width: 100%;
      display: inline-block;
      text-align: left;
    }
    .transaction_balance table tbody tr th, .transaction_balance table tbody tr td {
      border: 1px solid #cacaca;
    }
    #myTable1 td:last-child {
      border-bottom: 0 !important;
    }
    #myTable1 tbody tr {
      margin: 20px 0;
      display: inline-block;
      width: 100%;
      border: 1px solid #cacaca;
  }
  .transaction_balance table tbody tr th, .transaction_balance table tbody tr td {
      border-bottom: 1px solid #cecece !important;
  }
}
</style>
@endsection