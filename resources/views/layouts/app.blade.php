<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ Setting::get('site_title') }}</title>
<!-- <link rel="shortcut icon" type="image/png" href="{{ Setting::get('site_icon') }}"/> -->
<link rel="shortcut icon" type="image/png" href="{{ img(Setting::get('site_icon')) }}"/>

<link rel="icon" href="{{ Setting::get('site_icon') }}" sizes="16x16" type="image/png">
<!-- Font Awesome CSS -->
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- Ionicons CSS -->
<link href="ionicons/css/ionicons.min.css" rel="stylesheet">
<!-- Materail Design CSS -->
<link href="material-icons/css/materialdesignicons.min.css" rel="stylesheet">
<!-- Font -->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
<!-- lib -->
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/bootstrap-theme.min.css') }}" rel="stylesheet">
<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<!-- data table -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
<link href="{{ asset('css/animate.css') }}" rel="stylesheet">

<!-- custom style -->
<link href="{{ asset('css/style.css') }}" rel="stylesheet">
@yield('styles')
<style type="text/css">

</style>
<style type="text/css">
    .ethereum_wallet_alert_msg{
        padding: 15px;
        margin-bottom: 20px;
        border: 1px solid transparent;
        border-radius: 4px;
        text-align: center;
    }
    .ethereum_wallet_alert_msg span.btn-primary{ width: auto; border-radius: 3px; }
    .ethereum_wallet_alert_msg a {
        margin-left: 10px;
        display: inline-block;
    }
    #Ethereum-Modal{

    }
    #Ethereum-Modal .modal-content{ top: 0; box-shadow: 0 5px 15px rgba(255, 255, 255, .5);}
    #Ethereum-Modal .modal-header {
    background: #318205;
    color: #fff;
    }
    #Ethereum-Modal .modal-header h4.modal-title {
    color: #fff;
    }
    div#Ethereum-Modal .modal-header .close {
    color: #fff;
    opacity: 1;
    margin-top: 5px;
    }
    #Ethereum-Modal .modal-content .modal-header h4{ font-size: 20px; }
    .cannocar-steps ul {
    margin: 0;
    padding: 0;
    list-style: none;
    }
    .cannocar-steps ul li{
        position: relative;
        padding-left: 75px;
    }
    .cannocar-steps ul li + li {
    margin-top: 10px;
    }
    .cannocar-steps ul li a{
        color: #3097d1;
    }
    .cannocar-steps ul .bold {
    font-weight: bold;
    }

    .cannocar-steps ul li span.step-tl {
    display: block;
    position: absolute;
    left: 0px;
    font-size: 16px;
    }
    .cannocar-steps ul li span.note-txt1 {
    font-size: 13px;
    display: block;
    color: #f00;
    /* font-weight: bold; */
    }

</style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="logo navbar-brand" href="{{ url('/') }}">
                       <img src="/{{ Setting::get('site_logo') }}" height="50" alt="{{ Setting::get('site_title') }}">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li><a href="{{ url('/home') }}">Home</a></li>
                            <li><a href="{{ url('transaction') }}">Transactions</a></li>
                            <li><a href="{{ url('/referral') }}">Referral</a></li>
                            <li><a href="{{ url('/kyc') }}">KYC</a></li>
                            <li><a href="{{ url('/profile') }}">Profile</a></li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="GET" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="content-wrapper">
            @include('common.notify')
            @yield('content')
        </div>
        <style type="text/css">
            .effect5
            {
              
              background: #195260; height: 70px; width: 350px; position: fixed; bottom: 20px;left: 5px;
              /*box-shadow: 0 1px 15px 1px rgba(52,40,104,0.08);*/
              /*box-shadow: 11px 4px 25px 9px rgba(52,40,104,0.08);*/
              box-shadow: 0 0 10px rgba(0, 0, 0, .2);
              border: 1px solid #ccc;
              border-radius: 4px;
            }
            .offerpop_close{
              position: absolute;right: 5px;top:5px;                            
              background: #fff !important;
              background-size: 200% auto;
              font-weight: 600;
              transition: .5s;
              color: #000;
              border: 0 none;
              padding: 3px 8px;
              border-radius: 100px;
              font-size: 10px;
              line-height: 17px;
            }
            .offerpop_content {
                padding: 20px 30px;
                font-size: 20px;
            }
            .offerpop_content a{
              color: #fff;
            }

        </style>
        <div class="offerpop effect5" id="offerpop" style="display: none;">
          <a href="javascript:void(0);" class="offerpop_close" id="offerpop_close" onclick="myfuncofferpop_close();" >X</a>
          <div id="offerpop_content" class="offerpop_content"></div>
        </div>

 <!-- Modal -->
  <!-- <div class="modal fade" id="Ethereum-Modal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
    
     
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">How to Buy CannaCor Coins</h4>
        </div>
        <div class="modal-body">
          <div class="cannocar-steps">
            <ul>
                <li><span class="bold step-tl">Step 1</span> Download and Install the Atomic wallet
Follow this link to download the Atomic wallet: <a href="https://atomicwallet.io/downloads">https://atomicwallet.io/downloads</a> </li>
                <li><span class="bold step-tl">Step 2</span> Create your Atomic Wallet 
Follow this link for instructions: <a href="https://support.atomicwallet.io/article/7-how-to-set-up-a-wallet">https://support.atomicwallet.io/article/7-how-to-set-up-a-wallet</a> 
<span class="note-txt1">NB!! Copy and paste your 12-word phrase in a safe place. This is used to recover your account.</span></li>
                <li><span class="bold step-tl">Step 3</span> Buy Bitcoin or Ethereum 
Follow this link for instruction: <a href="https://support.atomicwallet.io/article/8-how-to-deposit-funds-to-atomic-wallet">https://support.atomicwallet.io/article/8-how-to-deposit-funds-to-atomic-wallet</a> </li>
                <li><span class="bold step-tl">Step 4</span> <span class="bold">Create your account on the CannaCor Platform</span> 
Follow this link to create your account: <a href="https://ico.cannacor.io/register">https://ico.cannacor.io/register</a> </li>
                <li><span class="bold step-tl">&nbsp;</span> a) Copy your Ethereum address from your Atomic wallet and add this to your profile <br> <br>Follow this link for instructions: <a href="https://support.atomicwallet.io/article/107-how-to-receive-an-asset-mobile">https://support.atomicwallet.io/article/107-how-to-receive-an-asset-mobile</a>
                </li>
                <li><span class="bold step-tl">&nbsp;</span> Follow steps 1 to 5 (ignore steps 6 to 8) on the link above, once you have successfully copied your Ethereum address, go to your profile on the CannaCor ICO dashboard and paste you Ethereum Address in the block under my ETH wallet </li>
                <li><span class="bold step-tl">&nbsp;</span> b) Submit your KYC documents <br>Please upload, </li>
                <li><span class="bold step-tl">&nbsp;</span> - A clear copy of your passport in JPEG format <<span class="note-txt1">NB! Please note that your account will be locked until KYC verification was successful. Your KYC will be approved in a couple of hours.</span> </li>
                <li><span class="bold step-tl">Step 6</span> How to purchase CannaCor (CANO) coins on the ICO dashboard? 
After you have registered and submitted your KYC, go to the home page on the CannaCor dashboard and follow steps 1 to 4. </li>
                <li><span class="bold step-tl">&nbsp;</span> - Select the currency (Bitcoin or Ethereum) that you have purchased in the Atomic wallet and click continue <br>
- Type in the amount of coins you want to purchase and click next step <br>
- Click on “copy to clipboard” <br>
- Send the Ethereum or Bitcoin from your Atomic wallet to the copied address. <br></li>
                <li><span class="bold step-tl">Step 7</span> How to send Bitcoin or Ethereum to the CannaCor ICO dashboard. 
Follow this link for instructions: <a href="https://support.atomicwallet.io/article/76-how-to-send-an-asset-in-atomic-wallet">https://support.atomicwallet.io/article/76-how-to-send-an-asset-in-atomic-wallet</a> </li>
                
            </ul>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div> -->
    </div>
    <!-- jquery.min.js -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <!-- Upload Doc -->
    <script type="text/javascript" src="{{ asset('js/jquery.uploadPreview.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('js/bootstrap.min.js') }} "></script>
    <!-- Data trable -->
    <script type="text/javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
      $(document).ready( function () {
        $('#myTable').DataTable();

        //offerpop();

      });
      $(document).ready( function () {
        $('#myTable1').DataTable();
      });

      function myfuncofferpop_close(){
        $("#offerpop").hide().fadeOut();
      }
      function offerpop(){
        $("#offerpop_content").html('');
        $.ajax({
          url: "{{url('/offerpop')}}",
          type: "GET",
          //data:{'usd':$('#priceusd').val(),'price':$('.price').val()}
        }).done(function(response){
          //console.log(response);
          //$('#offerpop').css('display','block');
          if(response!=0){
            $("#offerpop_content").html(response);
            $("#offerpop").show().delay(3000).fadeOut();
            /*$("#offerpop").toggle('slide', {
                direction: 'left'
            }, 500).fadeOut();*/
          }

        }).fail(function(jqXhr,status){

        });

        var minNumber = 10000;
        var maxNumber = 30000;

        var randomNumber = randomNumberFromRange(minNumber, maxNumber);

        //console.log(randomNumber);
        //setTimeout('offerpop()', 5000);
        setTimeout('offerpop()', randomNumber);
      }

      function randomNumberFromRange(min,max)
      {
          return Math.floor(Math.random()*(max-min+1)+min);
      }

    </script>
<script type="text/javascript">
    $('#Ethereum-Modal').modal({backdrop: 'static', keyboard: false})  
</script>
    <script src="{{ asset('js/scripts.js') }} "></script>

   

 <script>
     function getTimeRemaining(endtime) {
  var t = Date.parse(endtime) - Date.parse(new Date());
  // console.log(Date.parse(new Date()));
  // console.log(Date.parse(endtime));
  // console.log(t);
  var seconds = Math.floor((t / 1000) % 60);
  var minutes = Math.floor((t / 1000 / 60) % 60);
  var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  var days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    'total': t,
    'days': days,
    'hours': hours,
    'minutes': minutes,
    'seconds': seconds
  };
}

function initializeClock(id, endtime) {
  var clock = document.getElementById(id);
  var daysSpan = clock.querySelector('.days');
  var hoursSpan = clock.querySelector('.hours');
  var minutesSpan = clock.querySelector('.minutes');
  var secondsSpan = clock.querySelector('.seconds');

  function updateClock() {
    var t = getTimeRemaining(endtime);

    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    if (t.total <= 0) {
      clearInterval(timeinterval);
    }
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);
}

var deadline = new Date(Date.parse(new Date()) + 1 * 2 * 04 * 54 * 1000);
// console.log(deadline);
//initializeClock('clockdiv', deadline);
 </script>
 <script>
     document.querySelector("html").classList.add('js');

var fileInput  = document.querySelector( ".input-file" ),  
    button     = document.querySelector( ".input-file-trigger" ),
    the_return = document.querySelector(".file-return");
      
button.addEventListener( "keydown", function( event ) {  
    if ( event.keyCode == 13 || event.keyCode == 32 ) {  
        fileInput.focus();  
    }  
});
button.addEventListener( "click", function( event ) {
   fileInput.focus();
   return false;
});  
fileInput.addEventListener( "change", function( event ) {  
    the_return.innerHTML = this.value;  
});  
 </script>
 <script>
   
 </script>
   <!-- End of LiveChat code -->
    @yield('scripts')

</body>
</html>