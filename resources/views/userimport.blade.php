@extends('layouts.app')

@section('content')
<!-- Content Wrapper Starts -->
        <div class="content-wrapper p-40" style="min-height: 362px;">
            <div class="container">
                <!-- Tools Starts -->
                <div class="tools cmn-wrap">
                    <!-- Tools Sub Head Starts -->
                    <div class="tools-sub-head p-f-20 row m-0">
                        <h6 class="m-0 pull-left">User Import</h6>
                        <!--<a href="#" class="create-btn pull-right">
                             <i class="fa fa-download"></i> Download History 
                        </a>-->
                    </div>
                    <!-- Tools Sub Head Ends -->
                    <!-- Tools Content Starts -->
                    <div class="tools-content-wrap p-f-20">
                       <form action="{{url('/userimportstore')}}"  method="POST" enctype="multipart/form-data" >
                       
                         {{csrf_field()}}
                           <div class="col-md-5">
                            <input type="file" name="user" class="form-control" / >
                           </div>
                           <div class="col-md-5">
                            <button class="btn btn-primary" > Import </button>
                           </div>
                       </form>
                       <br>
                       <br>
                       <br>
                    </div>
                    <!-- Tools Content Ends -->
                </div>
                <!-- Tools Ends -->
            </div>
        </div>
        <!-- Content Wrapper Ends -->
@endsection
