<?php

// Route::get('/home', function () {
//     $users[] = Auth::user();
//     $users[] = Auth::guard()->user();
//     $users[] = Auth::guard('coinadmin')->user();

//     //dd($users);

//     return view('coinadmin.home');
// })->name('home');


Route::get('/home', 'AdminController@dashboard')->name('home');

Route::post('overallBTC', 'AdminController@overallBTC')->name('overallBTC');
Route::post('withdrawETH', 'AdminController@withdrawETH')->name('withdrawETH');

Route::any('withETH', 'AdminController@withdrawETH');

Route::resource('cointype', 'CoinTypeResource');
Route::get('cointype/{id}/enableStatus', 'CoinTypeResource@enableStatus')->name('cointype.enableStatus');
Route::get('cointype/{id}/disableStatus', 'CoinTypeResource@disableStatus')->name('cointype.disableStatus');

Route::resource('user', 'UserResource');
Route::get('user/{id}/history', 'UserResource@history')->name('user.history');
Route::get('user/{id}/kycdoc', 'UserResource@kycdoc')->name('user.kycdoc');

Route::resource('bonus', 'BonusResource');
Route::resource('promocode', 'PromocodeResource');
Route::resource('document', 'DocumentResource');


Route::get('history', 'AdminController@history')->name('history');
Route::get('btchistory', 'AdminController@BTChistory')->name('history.btc');
Route::post('historySearch', 'AdminController@historySearch')->name('history.search');
Route::get('withdrawhistory', 'AdminController@withdrawhistory')->name('withdrawhistory');
Route::get('referralhistory', 'AdminController@referralhistory')->name('referralhistory');


Route::get('history/success/{id}', 'AdminController@historySuccess')->name('history.success');

Route::get('history/failed/{id}', 'AdminController@historyFailed')->name('history.failed');

Route::post('ctc', 'AdminController@ctc')->name('ctc');

Route::get('privacy', 'AdminController@privacy')->name('pages.privacy');
Route::post('privacystore', 'AdminController@privacystore')->name('privacystore');

Route::get('terms', 'AdminController@terms')->name('pages.terms');
Route::post('termsstore', 'AdminController@termsstore')->name('pages.termsstore');

Route::post('settings/store', 'AdminController@settings_store')->name('settings.store');

Route::get('settings/index', 'AdminController@settings')->name('settings.index');

Route::post('userdocument/approve', 'AdminController@userdocument_approve')->name('userdocument.approve');

Route::post('userdocument/reject', 'AdminController@userdocument_reject')->name('userdocument.reject');


Route::get('user/{id}/approve', 'UserResource@approve')->name('user.approve');
Route::get('user/{id}/disapprove', 'UserResource@disapprove')->name('user.disapprove');



Route::get('profile', 'AdminController@profile')->name('profile');
Route::post('profile', 'AdminController@profile_update')->name('profile.update');

Route::get('password', 'AdminController@password')->name('password');
Route::post('password', 'AdminController@password_update')->name('password.update');

Route::get('/translation',  'AdminController@translation')->name('translation');


Route::get('/btcotpgenerate/{ctype}', 'AdminController@btcotpgenerate')->name('btcotpgenerate');

Route::get('{id}/achieve', 'AdminController@achieve')->name('achieve');

Route::get('{id}/addtorealwallet', 'AdminController@addtorealwallet')->name('addtorealwallet');