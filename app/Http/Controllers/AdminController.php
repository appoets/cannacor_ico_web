<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TransactionHistory;
use Setting;
use Storage;
use Auth;
use DB;
use GuzzleHttp\Client;
use App\Http\Controllers\HomeController;
use App\User;
use App\KycDocument;
use App\Coinadmin;
use App\WithdrawHistory;

use Mail;
use \Carbon\Carbon;
use App\Mail\Adminbtcotpmail;


class AdminController extends Controller
{

     public function dashboard()
    {
        try{

            $client = new Client;
            $xrpusd = $client->get('https://www.bitstamp.net/api/v2/ticker/xrpusd');
            $xrpusddetails = json_decode($xrpusd->getBody(),true);
            $xrpusddetails = $xrpusddetails['last'];

            $etherscan = $client->get('https://api.etherscan.io/api?module=stats&action=ethprice');
            $etherscandetails = json_decode($etherscan->getBody(),true);
            $etherscandetails = $etherscandetails['result']['ethusd'];

            $bitstamp = $client->get('https://www.bitstamp.net/api/v2/ticker/btcusd/');
            $bitstampdetails = json_decode($bitstamp->getBody(),true);
            $bitstampdetails = $bitstampdetails['last'];

            $History = TransactionHistory::with('user')->take(10)->orderBy('id','desc')->get();
            
            $name = "*";
            $param=[$name,1];
            $body = [                  
                'params' => $param,
                'method' => 'getbalance',        
            ];

            $curldata=$this->npmcurl($body);

            $Card['total_ico'] = User::sum(DB::raw('ico_balance')) + User::sum(DB::raw('ico_bonus'));
            $Card['total_btc'] = $curldata;
            $Card['total_eth'] = User::sum(DB::raw('ETH')); 
            $Card['total_xrp'] = User::sum(DB::raw('XRP'));

            return view('coinadmin.home',compact('bitstampdetails','etherscandetails','xrpusddetails','History', 'Card'));
        }
        catch(Exception $e){
            return back()->with('flash_error','Something Went Wrong with Dashboard!');
        }
    }
    public function history()
    {
        $History = TransactionHistory::with('user')->orderBy('id','desc')->get();
    	return view('coinadmin.history.index',compact('History'));
    }

    public function withdrawhistory()
    {
        $History = WithdrawHistory::orderBy('id','desc')->get();

        return view('coinadmin.history.withdraw',compact('History'));
    }

    public function referralhistory()
    {
        $user=User::where('referral_by','!=',null)->get();
        $tmp=array();
        foreach ($user as $key => $value) {
            array_push($tmp, $value['id']);
        }
        //$Transactions = TransactionHistory::whereIn('user_id',$val)->get();

        //$History = TransactionHistory::with('user')->orderBy('id','desc')->get();
        $History = TransactionHistory::whereIn('user_id',$tmp)->orderBy('id','desc')->get();

        return view('coinadmin.history.referral',compact('History'));
    }

    public function historySearch(Request $request)
    {
        $History = TransactionHistory::with('user');
        if($request->has('payment_mode')) {
            if($request->payment_mode != 'ALL') {
                $History->where('payment_mode', $request->payment_mode);
            }
        }
        if($request->has('referral')) {
            if($request->referral == 'REFERRAL') {
                $History->where('referral_by','!=',null);
            }
            if($request->referral == 'NON-REFERRAL') {
                $History->where('referral_by','=',null);
            }
        }
        if($request->has('from_date')) {
            $History->where('created_at', '>=', $request->from_date.' 00:00:00');
        }
        if($request->has('to_date')) {
            $History->where('created_at', '<=', $request->to_date.' 23:59:59');
        }
        if($request->has('tx_id')) {
            $History->where('payment_id', $request->tx_id);
        }
        $History = $History->orderBy('id','desc')->get();
        $Request = $request->all();

        return view('coinadmin.history.index',compact('History', 'Request'));
    }

    public function BTChistory(Request $request)
    {
        $History = TransactionHistory::with('user')->where('payment_mode', 'BTC')->orderBy('id','desc')->get();
        $Transactions = [];
        foreach ($History as $history) {
            $name = "rhicoinadminwallet";

            $param=[$history->address,0];
            $body = [
                'params' => $param,
                'method' => 'getreceivedbyaddress',
            ];

            $curldata=$this->npmcurl($body);
            if($curldata != $history->price) {
                $Transactions[] = $history;
            }
        }

        return view('coinadmin.history.btchistory',compact('Transactions'));
    }

    public function settings()
    {
        return view('coinadmin.settings.settings');
    }

    public function achieve($id)
    {
        $Trans = TransactionHistory::where('id',$id)->first();
        $Trans->achieve = '1';
        $Trans->save();

        return back()->with('flash_success','Successfully achieve');
    }

    public function addtorealwallet($id)
    {
        try{

            $Trans = TransactionHistory::where('id',$id)->first();

            // -------------------- Wallet amount update -----------------   
            /*$userwalletdata=[
                'email' => Auth::user()->email,
                'amount' => $request->amount_ctc,                
            ];
            $querystr=http_build_query($userwalletdata);
            $url="https://wallet.io/siteapi/guzzlewalletupdate?".$querystr;
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url);       
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
            $head = curl_exec($ch); 
            curl_close($ch);*/
            // -------------------- Wallet amount update -----------------

            $Trans->wallet_add_status = 1;
            $Trans->save();

            return back()->with('flash_success','Successfully Approved');
            
        }catch(Exception $e){
             return back()->with('flash_error','Something Went Wrong!');

        }
    }

    public function settings_store(Request $request)
    {

        $this->validate($request,[
                'site_title' => 'required',
                'site_icon' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                'site_logo' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                'coin_name' => 'required',
                'coin_symbol' => 'required',
                'coin_price' => 'required',
                'referral_bonus' => 'required',
                'coin_address' => 'required',
            ]);

        
        if($request->hasFile('site_icon')) {

            $site_icon = $request->site_icon->store('settings');
            Setting::set('site_icon', $site_icon);
        }

       

        if($request->hasFile('site_logo')) {
            $site_logo = $request->site_logo->store('settings');
            Setting::set('site_logo', 'storage/'.$site_logo);
        }

        if($request->hasFile('site_email_logo')) {
            $site_email_logo = $request->site_email_logo->store('settings');
            Setting::set('site_email_logo', 'storage/'.$site_email_logo);
        }

        Setting::set('site_title', $request->site_title);
        Setting::set('site_copyright', $request->site_copyright);
        // Setting::set('store_link_android', $request->store_link_android);
        // Setting::set('store_link_ios', $request->store_link_ios);
        Setting::set('coin_name', $request->coin_name);
        Setting::set('coin_symbol', $request->coin_symbol);
        Setting::set('coin_price', $request->coin_price);
        Setting::set('referral_bonus', $request->referral_bonus);
        Setting::set('coin_address', $request->coin_address);
        Setting::set('kyc_approval', $request->kyc_approval == 'on' ? 1 : 0 );
        Setting::set('referral_content', $request->referral_content);
        Setting::set('mail_id', $request->mail_id);
        Setting::set('fb_url', $request->fb_url);
        Setting::set('twt_url', $request->twt_url);
        Setting::set('telegram_url', $request->telegram_url);
        Setting::set('linkedin_url', $request->linkedin_url);
        Setting::set('medium_url', $request->medium_url);
       

      
        Setting::save();
        
        return back()->with('flash_success','Settings Updated Successfully');

    	
    }

     public function historySuccess($id)
    {
            $History = TransactionHistory::findOrFail($id);
            (new HomeController)->passbook($id);
            $History->status = "success";
            $History->save();

            return back()->with('flash_success','Settings Updated Successfully');
    }

     public function historyFailed($id)
    {
            $History = TransactionHistory::findOrFail($id);

            $History->status = "failed";
            $History->save();

            return back()->with('flash_success','Status Updated Successfully');
    }

    public function userdocument_approve(Request $request)
    {
        $Kyc = KycDocument::where('user_id',$request->user_id)
                    ->where('document_id',$request->doc_id)
                    ->first();

        $Kyc->status = $request->status;

        $Kyc->save();

         return back()->with('flash_success','Status Updated Successfully');

    }

    public function userdocument_reject(Request $request)
    {
        $Kyc = KycDocument::where('user_id',$request->user_id)
                    ->where('document_id',$request->doc_id)
                    ->first();

        $Kyc->status = $request->status;

        $Kyc->save();

         return back()->with('flash_success','Status Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $Transactions = TransactionHistory::with('user')->where('payment_mode', 'ETH')->where('withdraw', 0)->get();

            //$name = "rhicoinadminwallet";
            $name = "*";
            $param=[$name,1];
            $body = [                  
                'params' => $param,
                'method' => 'getbalance',        
            ];

            $curldata=$this->npmcurl($body);

            $coin = $curldata;

        return view('coinadmin.account.profile',compact('Transactions','coin'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function profile_update(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|max:255|email|unique:coinadmins,email,'.Auth::guard('coinadmin')->user()->id,
            'picture' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
        ]);

        try{
            $admin = Auth::guard('coinadmin')->user();
            $admin->name = $request->name;
            $admin->email = $request->email;
            
            if($request->hasFile('picture')){
                $admin->picture = $request->picture->store('admin/profile');  
            }
            $admin->save();

            return redirect()->back()->with('flash_success','Profile Updated');
        }

        catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function password()
    {
        return view('coinadmin.account.change-password');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function password_update(Request $request)
    {

        $this->validate($request,[
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        try {

           $Admin = Coinadmin::find(Auth::guard('coinadmin')->user()->id);

            if(password_verify($request->old_password, $Admin->password))
            {
                $Admin->password = bcrypt($request->password);
                $Admin->save();

                return redirect()->back()->with('flash_success','Password Updated');
            }
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

    public function translation(){

        try{
            return view('coinadmin.translation');
        }

        catch (Exception $e) {
             return back()->with('flash_error', trans('api.something_went_wrong'));
        }
    }


    public function overallBTC(Request $request)
    {

        if(isset($request->btc_otp)){

            $admin = Coinadmin::find(Auth::guard('coinadmin')->user()->id);

            if($request->btc_otp==$admin->btc_otp){

                //$name = "rhicoinadminwallet";
                $name = "*";
                $param=[$name,1];
                $body = [                  
                    'params' => $param,
                    'method' => 'getbalance',        
                ];

                $curldata=$this->npmcurl($body);

                $coin = $curldata;

                $param=[$name];
                $body = [
                    'params' => $param,
                    'method' => 'listtransactions',        
                ];
                $curldata=$this->npmcurl($body);

                $details1 = $curldata;


                $to_address = $request->btc;
                $coin1 = $request->amount;


                $name = $name;
                $param=[$name,$to_address, $coin1, 6];
                $body = [                  
                    'params' => $param,
                    'method' => 'sendfrom',        
                ];
                $curldata=$this->npmcurl($body);

                $details = $curldata;

                $Withdraw = new WithdrawHistory();
                $Withdraw->amount = $coin;
                $Withdraw->payment = "BTC";
                $Withdraw->address = $to_address;
                $Withdraw->save();

                $admin->btc_otp=Null;
                $admin->save;

                return back()->with('flash_success','Check your account !');

            }else{
                return back()->with('flash_error','Your otp is wrong!');
            }
        }else{
            return back()->with('flash_error','You should generate otp first!');
        }    
    }

    public function btcotpgenerate($ctype=null){

        $tmp=mt_rand(1000,9999);
        $coin_type=strtoupper($ctype);

        //$email="boopathi.tsm@gmail.com";     
        $email = Setting::get('mail_id');

        $maildata=[ 'otp'=>$tmp,'coin_type'=>$coin_type, ];

        $Admin = Coinadmin::find(Auth::guard('coinadmin')->user()->id);
        
        if($coin_type=='BTC'){
            $Admin->btc_otp=$tmp;
        }elseif($coin_type=='ETH'){
            $Admin->eth_otp=$tmp;
        }
        
        $Admin->save();


        Mail::to($email)->send(new Adminbtcotpmail($maildata));
    }


    public function npmcurl($body){
        
        try{
            $id=0;
            $status       = null;
            $error        = null;
            $raw_response = null;
            $response     = null;
            
            // $proto=env('BTC_PROTO');
            // $username =env('BTC_USERNAME');
            // $password =env('BTC_PASSWORD');
            // $host =env('BTC_HOST');
            // $host =env('BTC_HOST');
            // $port =env('BTC_PORT');
            $proto="http";
            $username ="because";
            $password ="because";
            $host ="127.0.0.1";
            $port ="8332";
            $url='';
            $CACertificate=null;
            $method=$body['method'];            
            // If no parameters are passed, this will be an empty array
            $params = $body['params'];
            $params = array_values($params);
            // The ID should be unique for each call
            $id++;
            // Build the request, it's ok that params might have any empty array
            $request = json_encode(array(
                'method' => $method,
                'params' => $params,
                'id'     => $id
            ));
            //$curl    = curl_init("{$proto}://{$host}:{$port}/{$url}");
            $curl    = curl_init("{$proto}://{$host}:{$port}/");
            $options = array(
                CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
                CURLOPT_USERPWD        => $username . ':' . $password,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_HTTPHEADER     => array('Content-type: application/json'),
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $request
            );
            // This prevents users from getting the following warning when open_basedir is set:
            // Warning: curl_setopt() [function.curl-setopt]:
            //   CURLOPT_FOLLOWLOCATION cannot be activated when in safe_mode or an open_basedir is set
            if (ini_get('open_basedir')) {
                unset($options[CURLOPT_FOLLOWLOCATION]);
            }
            
            if ($proto == 'https') {
               // If the CA Certificate was specified we change CURL to look for it
               if (!empty($CACertificate)) {
                   $options[CURLOPT_CAINFO] = $CACertificate;
                   $options[CURLOPT_CAPATH] = DIRNAME($CACertificate);
               } else {
                   // If not we need to assume the SSL cannot be verified
                   // so we set this flag to FALSE to allow the connection
                   $options[CURLOPT_SSL_VERIFYPEER] = false;
               }
           }
            curl_setopt_array($curl, $options);
            // Execute the request and decode to an array
            $raw_response = curl_exec($curl);
            $response     = json_decode($raw_response, true);
            // If the status is not 200, something is wrong
            $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            // If there was no error, this will be an empty string
            $curl_error = curl_error($curl);
            curl_close($curl);
            if (!empty($curl_error)) {
                $error = $curl_error;
            }
            if ($response['error']) {
                // If EINR returned an error, put that in $error
                $error = $response['error']['message'];
            } elseif ($status != 200) {
                // If EINR didn't return a nice error message, we need to make our own
                switch ($status) {
                    case 400:
                        $error = 'HTTP_BAD_REQUEST';
                        break;
                    case 401:
                        $error = 'HTTP_UNAUTHORIZED';
                        break;
                    case 403:
                        $error = 'HTTP_FORBIDDEN';
                        break;
                    case 404:
                        $error = 'HTTP_NOT_FOUND';
                        break;
                }
            }
            if ($error) {
                return false;
            }
             //dd($response);
            return $response['result'];
        }catch(Exception $e){

        }
    }

    public function withdrawETH(Request $request)
    {

        if(isset($request->eth_otp)){

            $admin = Coinadmin::find(Auth::guard('coinadmin')->user()->id);

            if($request->eth_otp==$admin->eth_otp){

                $History = TransactionHistory::with('user')->whereIn('id', $request->ids)->get();
                foreach ($History as $history) {

                    $client = new Client();
                    $url ="http://localhost:3000/transferETH";                
                    $headers = [
                        'Content-Type' => 'application/json',
                    ];
                    $body = ["senderAddress" => $history->address,"senderPassword" => $history->user->email,"toAddress" => $request->eth,"amount" => $history->price];

                    $res = $client->post($url, [
                        'headers' => $headers,
                        'body' => json_encode($body),
                    ]);
                    $details = json_decode($res->getBody(),true);  
                    \Log::info($details);
                    if($details['status'] == 'success')
                    {
                        $history->withdraw = 1;
                        $history->save();

                        $Withdraw = new WithdrawHistory();
                        $Withdraw->amount = $history->price;
                        $Withdraw->payment = "ETH";
                        $Withdraw->address = $request->eth;
                        $Withdraw->save();                        
                    }
                    else{
                        return back()->with('flash_error', 'Gas Price is too high. Please try again later');
                    }                    
                    /*$headers = [
                        'Content-Type' => 'application/json',
                    ];
                    $body = ["jsonrpc" => "2.0","method" => "personal_unlockAccount", "params" => [$history->address, $history->user->email, 3600], "id" => 1];
                    $url ="http://localhost:8545";
                    $res = $client->post($url, [
                        'headers' => $headers,
                        'body' => json_encode($body),
                    ]);
                    $unlock = json_decode($res->getBody(),true);
                    \Log::info($unlock);
                    if($unlock['result']) {

                        $price_num = \DB::select('select ('.    $history->price.'-0.000084) * 1000000000000000000');
                        $arr = json_decode(json_encode($price_num[0]), true);
                        $final_num=current($arr);
                        $price = \DB::select('select CONV('.$final_num.',10,16)');
                        $arr = json_decode(json_encode($price[0]), true);
                        $price='0x'.current($arr);
                        //$price = '0x'.dechex(($history->price-0.000084) * 1000000000000000000);

                        $client = new Client();
                        $headers = [
                            'Content-Type' => 'application/json',
                        ];
                        $body = ["jsonrpc" => "2.0","method" => "eth_sendTransaction", "params" => array(["from" => $history->address, "to" => $request->eth, "gas" => "0x5208", "gasPrice" => "0xEE6B2800", "value" => $price]), "id" => 1];

                        $res = $client->post($url, [
                            'headers' => $headers,
                            'body' => json_encode($body),
                        ]);
                        $txs = json_decode($res->getBody(),true);
                        \Log::info($txs);
                        if(isset($txs['result'])) {
                            $history->withdraw = 1;
                            $history->save();

                            $Withdraw = new WithdrawHistory();
                            $Withdraw->amount = hexdec($price) / 1000000000000000000;
                            $Withdraw->payment = "ETH";
                            $Withdraw->address = $request->eth;
                            $Withdraw->save();
                        }
                        else
                            return back()->with('flash_error', 'Gas Price is too high. Please try again later');
                    }*/
                }

                $admin->eth_otp=Null;
                $admin->save;

                return back()->with('flash_success','Check your account !');
            }else{
                return back()->with('flash_error','Your otp is wrong!');
            }
        }else{
            return back()->with('flash_error','You should generate otp first!');
        }
    }

    /*public function guzzlepostuser(){

        $client = new Client();
        $headers = [
            'Content-Type' => 'application/json',
        ];
        $body = ["method" => "personal_newAccount", "id" => 1];

       
        $url="https://142.93.88.7/siteapi/guzzlesaveuser";
        $data=['headers' => $headers,'body' => json_encode($body)];

        
        //$res = $client->post($url, $data);
        $res = $client->get($url,['auth'=>['username','password']]);
        $eth_address = json_decode($res->getBody(),true);

    }*/

    
    public function guzzlesaveuser(Request $request){
        
        try{
            $userold=User::where('email','=',$request['email'])->first();

            if(count($userold)==0){

                /*$name = "rhicoinadminwallet";
            
                $client = new Client();
                $headers = [
                    'Content-Type' => 'application/json',
                ];
                $body = ["method" => "personal_newAccount", "params" => [$request['email']], "id" => 1];

                $url ="http://localhost:8545";


                $res = $client->post($url, [
                    'headers' => $headers,
                    'body' => json_encode($body),
                ]);
                $eth_address = json_decode($res->getBody(),true);

                $param = [$name];

                $body = ['params' => $param, 'method' => 'getnewaddress'];
                //$btc_address = (new HomeController)->npmcurl($body);
                $btc_address = $this->npmcurl($body);*/

                $userdata=[
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'password' => bcrypt($request['password']),
                    'country_id' => 103,
                    'referral_by' => Null,
                    //'eth_address' => $eth_address['result'],
                    //'btc_address' => $btc_address,
                    /*'eth_address' => 'eth_address',
                    'btc_address' => 'btc_address',*/
                    'email_token' => base64_encode($request['email']),
                    'ip' => $request['ip'],
                ];
                    
                //return json_decode($userdata,true);

                User::create($userdata);
            }

            return 1;


        }catch(Exception $e){

            return 0;
        }
    }

    public function guzzleverifyuser(Request $request){
        
        try{
            $user=User::where('email','=',$request['email'])->first();

            if(count($user)==1){
                $user->verified = 1;
                $user->save();               
            }

            return 1;


        }catch(Exception $e){

            return 0;
        }
    }


    /**
     * privacy.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */

    public function privacy(){
        return view('coinadmin.pages.privacy')
        ->with('title',"Privacy Page")
        ->with('page', "privacy");
    }

    public function privacystore(Request $request){
        $this->validate($request,[
            'content' => 'required',
        ]);

        try {

            Setting::set('privacy', $request->content);       
            Setting::save();
        
            return back()->with('flash_success','Privacy Updated Successfully');

                
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

    public function terms(){
        return view('coinadmin.pages.terms')
        ->with('title',"Terms Page")
        ->with('page', "Terms");
    }


    public function termsstore(Request $request){
        $this->validate($request,[
            'content' => 'required',
        ]);

        try {

            Setting::set('terms', $request->content);       
            Setting::save();
        
            return back()->with('flash_success','Terms Updated Successfully');

                
        } catch (Exception $e) {
             return back()->with('flash_error','Something Went Wrong!');
        }
    }

    /**
     * pages.
     *
     * @param  \App\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function pages(Request $request){
        $this->validate($request, [
                'page' => 'required|in:page_privacy',
                'content' => 'required',
            ]);

        Setting::set($request->page, $request->content);
        Setting::save();

        return back()->with('flash_success', 'Content Updated!');
    }
}

    /**
     * account statements.
     *


   
}
