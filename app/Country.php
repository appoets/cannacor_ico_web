<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function countrycurrency()
    {
       
        return $this->hasMany('App\CountryCurrency', 'country_id');
    }

    public function countrycrypto()
    {
        //return $this->hasOne('App\CountryCryptoCurrency', 'country_id');
        return $this->hasMany('App\CountryCryptoCurrency', 'country_id');
    }

    public function countrypayment()
    {
        //return $this->hasOne('App\CountryCryptoCurrency', 'country_id');
        return $this->hasMany('App\CountryPaymentMethod', 'country_id');
    }
}
