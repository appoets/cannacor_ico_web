<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_histories', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id');
            $table->string('ico');
            $table->string('ico_initial');
            $table->string('ico_price');
            $table->string('price');
            $table->string('payment_mode');
            $table->string('bonus_point');
            $table->string('referal_code');
            $table->integer('referral_by')->nullable();
            $table->string('referral_percentage')->nullable();
            $table->string('referral_amount')->nullable();
            $table->string('address')->nullable();
            $table->integer('withdraw')->default(0);
            $table->text('payment_id')->nullable();
            $table->string('promocode')->nullable();
            $table->string('usd')->nullable();
            $table->enum('status', [
                    'pending',
                    'processing',
                    'failed',
                    'success',
                ])->default('pending');
            $table->enum('achieve', [
                    '0',
                    '1',
                   
                ])->default('0');
            $table->tinyInteger('wallet_add_status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_histories');
    }
}
